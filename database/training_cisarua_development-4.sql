-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jul 2020 pada 12.44
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_cisarua_development`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE `cities` (
  `id` int(10) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `province_id` int(10) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `get_roles_access`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `get_roles_access` (
`id` int(5)
,`role_id` int(5)
,`module_application_id` int(5)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
,`user_id` int(10) unsigned
,`path` varchar(25)
,`module_id` int(5)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `get_role_modules`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `get_role_modules` (
`role_id` int(11)
,`role_module_id` int(5)
,`module_application_id` int(5)
,`module_id` int(5)
,`module_name` varchar(20)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `invoice_no` varchar(12) NOT NULL,
  `total_amount` float NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'not_paid_yet',
  `file_upload` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `invoices`
--

INSERT INTO `invoices` (`id`, `training_id`, `invoice_no`, `total_amount`, `status`, `file_upload`, `user_id`, `created_at`, `delete`) VALUES
(1, 3, '#i03', 10475000, 'not_paid_yet', '', 4, '2020-07-03', 'active'),
(2, 4, '#i-04', 500000, 'approve', 'quizsql.PNG', 4, '2020-07-03', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mdfacilities`
--

CREATE TABLE `mdfacilities` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `perunit` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mdfacilities`
--

INSERT INTO `mdfacilities` (`id`, `name`, `price`, `perunit`, `description`, `created_at`, `delete`) VALUES
(1, 'Sewa Aula', 850000, 'Pemakaian', 'lorem ipsum', '2020-07-02', 'active'),
(2, 'Sewa Kelas', 25000, 'Mg/Mhs', 'penyewaan kelas', '2020-07-02', 'active'),
(3, 'Sewa Asrama/Mess', 15000, 'Mhs/Hr', 'sewa asrama untuk mahasiswa pehari dikenakan biaya Rp 15.000', '2020-07-03', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mdrooms`
--

CREATE TABLE `mdrooms` (
  `id` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `room_capacity` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mdrooms`
--

INSERT INTO `mdrooms` (`id`, `room_name`, `room_capacity`, `description`, `created_at`, `delete`) VALUES
(1, 'Nuri', '30', 'nama ruangan', '0000-00-00', 'active'),
(2, 'Elang', '30', 'Nama ruangan', '2020-07-08', 'active'),
(3, 'Rajawali', '33', 'nama ruangan', '2020-07-08', 'active'),
(4, 'Garuda', '24', 'nama dan kapasitas ruangan\r\n', '2020-07-09', 'active'),
(5, 'Keswara', '30', 'nama dan kapasitas ruangan', '2020-07-09', 'active'),
(6, 'Galatik', '24', 'nama dan kapasitas ruangan', '2020-07-09', 'active'),
(7, 'Cendrawasih', '30', 'nama dan kapasitas ruangan', '2020-07-09', 'active'),
(8, 'Perkutut', '66', 'nama dan kapasitas ruangan', '2020-07-09', 'active'),
(9, 'Merak', '60', 'nama dan kapasitas ruangan', '2020-07-09', 'active'),
(10, 'Merpati', '70', 'nama dan kapasitas ruangan', '2020-07-09', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mdroom_users`
--

CREATE TABLE `mdroom_users` (
  `id` int(11) NOT NULL,
  `mdroom_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `modules`
--

CREATE TABLE `modules` (
  `id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `path_module` varchar(25) NOT NULL,
  `module_class` varchar(25) NOT NULL,
  `created_at` date NOT NULL,
  `active_class` varchar(40) NOT NULL,
  `child` int(11) DEFAULT NULL,
  `model_class` varchar(25) NOT NULL,
  `menu_order` int(3) NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `path_module`, `module_class`, `created_at`, `active_class`, `child`, `model_class`, `menu_order`, `delete`) VALUES
(9, 'Master Data', 'lorem ipsum', '#', 'fa fa-table', '2018-04-18', 'open_data_master', NULL, '', 1, 'active'),
(10, 'Pengguna', '', 'users', 'fa fa-circle-o', '2018-04-18', 'users', 9, 'User_model', 0, 'active'),
(11, 'Hak Akses', 'lorem ipsum dolor sit amet', 'roles', 'fa fa-circle-o', '2018-04-18', 'roles', 9, 'Role_model', 0, 'active'),
(12, 'Module', 'lorem ipsum dolor sit amet', 'modules', 'fa fa-circle-o', '2018-04-18', 'modules', 9, 'Module_model', 0, 'active'),
(13, 'Fasilitas', 'Master data fasilitas', 'mdfacilities', 'fa fa-circle-o', '2020-07-02', 'mdfacilities', 9, 'Mdfacility_model', 0, 'active'),
(14, 'Sistem Penyewaan', 'Untuk diklat', '#', 'fa fa-home', '2020-07-02', 'open_data_system', NULL, '-', 0, 'active'),
(16, 'Diklat', 'Untuk diklat', 'trainings', 'fa fa-circle-o', '2020-07-02', 'trainings', 14, 'Mdtraining_model', 2, 'active'),
(17, 'Transaksi', 'Untuk transaksi', '#', 'fa fa-money', '2020-07-03', 'open_data_sales', NULL, '-', 0, 'active'),
(18, 'Faktur', '', 'invoices', 'fa fa-circle-o', '2020-07-03', 'invoices', 17, 'Invoice_model', 0, 'active'),
(19, 'Kwitansi', 'Untuk kwitansi', 'receipts', 'fa fa-circle-o', '2020-07-03', 'receipts', 17, 'Receipt_model', 0, 'active'),
(20, 'Akademik', '', '#', 'fa fa-book', '2020-07-03', 'open_data_administration', NULL, '-', 0, 'active'),
(21, 'Mahasiswa', 'untuk mahasiswa', 'students', 'fa fa-circle-o', '2020-07-03', 'students', 20, 'Student_model', 0, 'active'),
(22, 'Jadwal', 'untuk jadwal', 'schedules', 'fa fa-circle-o', '2020-07-03', 'schedules', 20, 'Schedule_model', 0, 'active'),
(25, 'Ruangan', 'Master Data Ruangan', 'mdrooms', 'fa fa-circle-o', '2020-07-08', 'mdrooms', 9, 'mdroom_model', 0, 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `module_applications`
--

CREATE TABLE `module_applications` (
  `id` int(5) NOT NULL,
  `module_id` int(5) NOT NULL,
  `action_read` varchar(30) NOT NULL,
  `action_create` varchar(30) NOT NULL,
  `action_update` varchar(30) NOT NULL,
  `action_destroy` varchar(30) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `module_applications`
--

INSERT INTO `module_applications` (`id`, `module_id`, `action_read`, `action_create`, `action_update`, `action_destroy`, `created_at`) VALUES
(3, 9, 'can :read, ', 'can :create, ', 'can :update, ', 'can :destroy, ', '0000-00-00'),
(4, 10, 'can :read, User_model', 'can :create, User_model', 'can :update, User_model', 'can :destroy, User_model', '0000-00-00'),
(5, 11, 'can :read, Role_model', 'can :create, Role_model', 'can :update, Role_model', 'can :destroy, Role_model', '0000-00-00'),
(6, 12, 'can :read, Module_model', 'can :create, Module_model', 'can :update, Module_model', 'can :destroy, Module_model', '0000-00-00'),
(7, 13, 'can :read, Mdfacility_model', 'can :create, Mdfacility_model', 'can :update, Mdfacility_model', 'can :destroy, Mdfacility_model', '0000-00-00'),
(8, 14, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(10, 16, 'can :read, Facility_model', 'can :create, Facility_model', 'can :update, Facility_model', 'can :destroy, Facility_model', '0000-00-00'),
(11, 17, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(12, 18, 'can :read, Invoice_model', 'can :create, Invoice_model', 'can :update, Invoice_model', 'can :destroy, Invoice_model', '0000-00-00'),
(13, 19, 'can :read, Receipt_model', 'can :create, Receipt_model', 'can :update, Receipt_model', 'can :destroy, Receipt_model', '0000-00-00'),
(14, 20, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(15, 21, 'can :read, Student_model', 'can :create, Student_model', 'can :update, Student_model', 'can :destroy, Student_model', '0000-00-00'),
(16, 22, 'can :read, Schedule_model', 'can :create, Schedule_model', 'can :update, Schedule_model', 'can :destroy, Schedule_model', '0000-00-00'),
(17, 23, 'can :read, payment_proofs_mode', 'can :create, payment_proofs_mo', 'can :update, payment_proofs_mo', 'can :destroy, payment_proofs_m', '0000-00-00'),
(18, 24, 'can :read, facilities', 'can :create, facilities', 'can :update, facilities', 'can :destroy, facilities', '0000-00-00'),
(19, 24, 'can :read, mdroom_model', 'can :create, mdroom_model', 'can :update, mdroom_model', 'can :destroy, mdroom_model', '0000-00-00'),
(20, 25, 'can :read, mdroom_model', 'can :create, mdroom_model', 'can :update, mdroom_model', 'can :destroy, mdroom_model', '0000-00-00'),
(21, 25, 'can :read, mdroom_model', 'can :create, mdroom_model', 'can :update, mdroom_model', 'can :destroy, mdroom_model', '0000-00-00'),
(22, 22, 'can :read, Schedule_model', 'can :create, Schedule_model', 'can :update, Schedule_model', 'can :destroy, Schedule_model', '0000-00-00'),
(23, 21, 'can :read, Student_model', 'can :create, Student_model', 'can :update, Student_model', 'can :destroy, Student_model', '0000-00-00'),
(24, 20, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(25, 19, 'can :read, Receipt_model', 'can :create, Receipt_model', 'can :update, Receipt_model', 'can :destroy, Receipt_model', '0000-00-00'),
(26, 18, 'can :read, Invoice_model', 'can :create, Invoice_model', 'can :update, Invoice_model', 'can :destroy, Invoice_model', '0000-00-00'),
(27, 17, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(28, 16, 'can :read, Mdtraining_model', 'can :create, Mdtraining_model', 'can :update, Mdtraining_model', 'can :destroy, Mdtraining_model', '0000-00-00'),
(29, 14, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(30, 13, 'can :read, Mdfacility_model', 'can :create, Mdfacility_model', 'can :update, Mdfacility_model', 'can :destroy, Mdfacility_model', '0000-00-00'),
(31, 12, 'can :read, Module_model', 'can :create, Module_model', 'can :update, Module_model', 'can :destroy, Module_model', '0000-00-00'),
(32, 11, 'can :read, Role_model', 'can :create, Role_model', 'can :update, Role_model', 'can :destroy, Role_model', '0000-00-00'),
(33, 10, 'can :read, User_model', 'can :create, User_model', 'can :update, User_model', 'can :destroy, User_model', '0000-00-00'),
(34, 9, 'can :read, ', 'can :create, ', 'can :update, ', 'can :destroy, ', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) NOT NULL,
  `province` varchar(100) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `receipts`
--

CREATE TABLE `receipts` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `for_payment` varchar(50) NOT NULL,
  `responsibility` varchar(30) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'pending',
  `receipt_no` varchar(12) NOT NULL,
  `recomendation_no` varchar(12) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `receipts`
--

INSERT INTO `receipts` (`id`, `invoice_id`, `training_id`, `receiver`, `for_payment`, `responsibility`, `nip`, `status`, `receipt_no`, `recomendation_no`, `description`, `created_at`, `delete`) VALUES
(1, 2, 4, 'sad', 'fsdfsaf', 'adas', '235254645', 'pending', 'uidhf', 'asd', 'dadasdasd', '2020-07-07', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `delete`, `created_at`) VALUES
(1, 'Superadmin', 'superadmin', 'active', '2018-04-16'),
(2, 'Admin', 'lorem', 'deleted', '2018-04-16'),
(3, 'Import Master', 'loremasita', 'deleted', '2018-04-16'),
(4, 'Admin', 'Admin', 'active', '2020-07-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `module_application_id` int(5) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '1',
  `is_add` tinyint(1) NOT NULL DEFAULT '1',
  `is_update` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_application_id`, `is_read`, `is_add`, `is_update`, `is_delete`, `created_at`) VALUES
(2, 1, 3, 1, 1, 1, 1, '0000-00-00'),
(3, 1, 4, 1, 1, 1, 0, '0000-00-00'),
(4, 1, 5, 1, 1, 1, 0, '0000-00-00'),
(5, 1, 6, 1, 1, 1, 0, '0000-00-00'),
(6, 1, 7, 1, 1, 1, 1, '0000-00-00'),
(7, 1, 8, 1, 1, 1, 1, '0000-00-00'),
(9, 1, 10, 1, 1, 1, 1, '0000-00-00'),
(10, 1, 11, 1, 1, 1, 1, '0000-00-00'),
(11, 1, 12, 1, 1, 1, 1, '0000-00-00'),
(12, 1, 13, 1, 1, 1, 1, '0000-00-00'),
(13, 1, 14, 1, 1, 1, 1, '0000-00-00'),
(14, 1, 15, 1, 1, 1, 1, '0000-00-00'),
(15, 1, 16, 1, 1, 1, 1, '0000-00-00'),
(16, 1, 17, 0, 0, 0, 0, '0000-00-00'),
(17, 1, 18, 1, 1, 1, 1, '0000-00-00'),
(18, 1, 19, 1, 1, 1, 1, '0000-00-00'),
(19, 1, 20, 1, 1, 1, 1, '0000-00-00'),
(20, 4, 20, 0, 1, 1, 1, '0000-00-00'),
(21, 4, 19, 1, 1, 1, 1, '0000-00-00'),
(22, 4, 18, 1, 1, 1, 1, '0000-00-00'),
(23, 4, 17, 1, 1, 1, 1, '0000-00-00'),
(24, 4, 16, 1, 1, 1, 1, '0000-00-00'),
(25, 4, 15, 1, 1, 1, 1, '0000-00-00'),
(26, 4, 14, 1, 1, 1, 1, '0000-00-00'),
(27, 4, 13, 1, 1, 1, 1, '0000-00-00'),
(28, 4, 12, 1, 1, 1, 1, '0000-00-00'),
(29, 4, 11, 1, 1, 1, 1, '0000-00-00'),
(30, 4, 10, 1, 1, 1, 1, '0000-00-00'),
(31, 4, 8, 1, 1, 1, 1, '0000-00-00'),
(32, 4, 7, 0, 1, 1, 1, '0000-00-00'),
(33, 4, 6, 0, 1, 1, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  ` schedule` varchar(50) NOT NULL,
  `mentor_name` varchar(50) NOT NULL,
  `program_study` varchar(50) NOT NULL,
  `mdroom_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `schedules`
--

INSERT INTO `schedules` (`id`, ` schedule`, `mentor_name`, `program_study`, `mdroom_id`, `description`, `created_at`, `delete`) VALUES
(1, '', 'Hj. Nenti, S.Kep., Ners', 'S1 / NERS', 1, 'Lorem', '2020-07-09', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `nim` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `instance` varchar(50) NOT NULL,
  `majors` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `students`
--

INSERT INTO `students` (`id`, `training_id`, `name`, `nim`, `email`, `instance`, `majors`, `description`, `created_at`, `delete`) VALUES
(1, 2, 'aldy', 1234, 'aldy@gmail.com', 'UNISBA', 'STIKES', '', '2020-07-07', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `major` varchar(20) NOT NULL,
  `semester` varchar(5) NOT NULL,
  `training_specification` text NOT NULL,
  `accreditation` varchar(5) NOT NULL DEFAULT 'A',
  `no_mou` varchar(20) NOT NULL,
  `date_cooperation_start` date NOT NULL,
  `date_cooperation_end` date NOT NULL,
  `address` text NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `training_date_start` date NOT NULL,
  `training_date_end` date NOT NULL,
  `participants` int(20) NOT NULL,
  `approve_date` date NOT NULL,
  `mentor` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'pending',
  `file_proposal` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trainings`
--

INSERT INTO `trainings` (`id`, `name`, `major`, `semester`, `training_specification`, `accreditation`, `no_mou`, `date_cooperation_start`, `date_cooperation_end`, `address`, `contact_person`, `phone`, `email`, `training_date_start`, `training_date_end`, `participants`, `approve_date`, `mentor`, `status`, `file_proposal`, `user_id`, `created_at`, `delete`) VALUES
(2, 'Diklat 1', 'Psikologi', 'S2', 'Praktek Klinik Jiwa D3', 'B', '001', '2020-07-01', '2020-07-30', 'lorem ipsum', 'Hasan A.F', '0019', 'alfarisisholehh@yahoo.com', '2020-07-01', '2020-07-10', 100, '0000-00-00', 'Hasan', 'submit', '', 4, '2020-07-02', 'active'),
(3, 'Diklat 2', 'Psikologi', 'D3', 'ProgramProfesi NERS', '', '002', '2020-07-01', '2020-07-30', 'lorem', 'Hasan A.F', '0019', 'alfarisisholehh@yahoo.com', '2020-07-02', '2020-07-15', 151, '0000-00-00', 'Hasan', 'approve', '', 4, '2020-07-02', 'active'),
(4, 'Diklat 3', 'Pekerja Sosial', 'D2', 'Profesi Farmasi', 'C', '123456', '2017-07-14', '2017-07-15', 'Jl. Letkol G.A Manulang No.214\r\n214', '40553', '0895328679873', 'sadrclothes@gmail.com', '2017-07-15', '2017-07-15', 100, '0000-00-00', 'HASAN', 'approve', '', 4, '2020-07-03', 'active'),
(5, 'test', 'Kedokteran', 'SMA', 'Pendidikan Sarjana Kedokteran', 'C', '1234', '2020-07-04', '2020-07-05', '', 'ksjfhsk', '984619', 'devadm@jgia.com', '2020-07-06', '2020-07-07', 110, '0000-00-00', 'adasds', 'submit', '', 4, '2020-07-03', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `training_facilities`
--

CREATE TABLE `training_facilities` (
  `id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `frequency` int(5) NOT NULL,
  `created_at` int(11) NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `training_facilities`
--

INSERT INTO `training_facilities` (`id`, `training_id`, `facility_id`, `price`, `frequency`, `created_at`, `delete`) VALUES
(1, 3, 2, 250000, 10, 2020, 'active'),
(2, 3, 1, 10200000, 12, 2020, 'active'),
(5, 2, 2, 250000, 10, 2020, 'active'),
(6, 4, 2, 500000, 20, 2020, 'active'),
(7, 5, 3, 1515000, 101, 2020, 'active'),
(8, 5, 2, 2525000, 101, 2020, 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city_id` int(10) NOT NULL,
  `province_id` int(10) NOT NULL,
  `role_id` int(5) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `phone`, `address`, `city_id`, `province_id`, `role_id`, `created_at`, `delete`) VALUES
(1, 'Loreal', 'andoyoandoyo@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 1, '2018-04-16', 'deleted'),
(2, 'Lorem ipsum dolor Hasan', 'dolor@lorem.com', 'lorem_ipsum', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 1, '2018-04-03', 'deleted'),
(3, 'Hasan Al Farisi', 'learn@kiranatama.com', 'lorem', 'd2e16e6ef52a45b7468f1da56bba1953', '', '', 0, 0, 1, '2018-04-15', 'deleted'),
(4, 'Hasan Al Farisi', 'learn@gmail.com', 'admin', '25d55ad283aa400af464c76d713c07ad', '', '', 0, 0, 1, '2018-04-16', 'active'),
(5, 'role_percobaan', 'role@role.com', 'rolade', 'a516ff74e222c3bbd3ab924e96fefe0c', '', '', 0, 0, 1, '2018-04-19', 'deleted');

-- --------------------------------------------------------

--
-- Struktur untuk view `get_roles_access`
--
DROP TABLE IF EXISTS `get_roles_access`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_roles_access`  AS  select `role_modules`.`id` AS `id`,`role_modules`.`role_id` AS `role_id`,`role_modules`.`module_application_id` AS `module_application_id`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete`,`users`.`id` AS `user_id`,`modules`.`path_module` AS `path`,`modules`.`id` AS `module_id` from ((((`role_modules` join `roles` on((`roles`.`id` = `role_modules`.`role_id`))) join `users` on((`users`.`role_id` = `roles`.`id`))) join `module_applications` on((`module_applications`.`id` = `role_modules`.`module_application_id`))) join `modules` on((`modules`.`id` = `module_applications`.`module_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `get_role_modules`
--
DROP TABLE IF EXISTS `get_role_modules`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_role_modules`  AS  select `roles`.`id` AS `role_id`,`role_modules`.`id` AS `role_module_id`,`module_applications`.`id` AS `module_application_id`,`modules`.`id` AS `module_id`,`modules`.`name` AS `module_name`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete` from (((`role_modules` join `module_applications` on((`module_applications`.`id` = `role_modules`.`module_application_id`))) join `modules` on((`modules`.`id` = `module_applications`.`module_id`))) join `roles` on((`roles`.`id` = `role_modules`.`role_id`))) where (`modules`.`child` is not null) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mdfacilities`
--
ALTER TABLE `mdfacilities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mdrooms`
--
ALTER TABLE `mdrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mdroom_users`
--
ALTER TABLE `mdroom_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `module_applications`
--
ALTER TABLE `module_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `training_facilities`
--
ALTER TABLE `training_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `mdfacilities`
--
ALTER TABLE `mdfacilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `mdrooms`
--
ALTER TABLE `mdrooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `mdroom_users`
--
ALTER TABLE `mdroom_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `module_applications`
--
ALTER TABLE `module_applications`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `training_facilities`
--
ALTER TABLE `training_facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
