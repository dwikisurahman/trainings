-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 03, 2020 at 05:31 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_cisarua_development`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `get_roles_access`
-- (See below for the actual view)
--
CREATE TABLE `get_roles_access` (
`id` int(5)
,`role_id` int(5)
,`module_application_id` int(5)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
,`user_id` int(10) unsigned
,`path` varchar(25)
,`module_id` int(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `get_role_modules`
-- (See below for the actual view)
--
CREATE TABLE `get_role_modules` (
`role_id` int(11)
,`role_module_id` int(5)
,`module_application_id` int(5)
,`module_id` int(5)
,`module_name` varchar(20)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `invoice_no` varchar(12) NOT NULL,
  `total_amount` float NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'not_paid_yet',
  `file_upload` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `training_id`, `invoice_no`, `total_amount`, `status`, `file_upload`, `user_id`, `created_at`, `delete`) VALUES
(1, 3, '#i03', 10475000, 'not_paid_yet', '', 4, '2020-07-03', 'active'),
(2, 4, '#i-04', 500000, 'not_paid_yet', '', 4, '2020-07-03', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `mdfacilities`
--

CREATE TABLE `mdfacilities` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `perunit` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mdfacilities`
--

INSERT INTO `mdfacilities` (`id`, `name`, `price`, `perunit`, `description`, `created_at`, `delete`) VALUES
(1, 'Sewa Aula', 850000, 'Pemakaian', 'lorem ipsum', '2020-07-02', 'active'),
(2, 'Sewa Kelas', 25000, 'Mg/Mhs', 'penyewaan kelas', '2020-07-02', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `path_module` varchar(25) NOT NULL,
  `module_class` varchar(25) NOT NULL,
  `created_at` date NOT NULL,
  `active_class` varchar(40) NOT NULL,
  `child` int(11) DEFAULT NULL,
  `model_class` varchar(25) NOT NULL,
  `menu_order` int(3) NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `path_module`, `module_class`, `created_at`, `active_class`, `child`, `model_class`, `menu_order`, `delete`) VALUES
(9, 'Master Data', 'lorem ipsum', '#', 'fa fa-table', '2018-04-18', 'open_data_master', NULL, '', 1, 'active'),
(10, 'Pengguna', '', 'users', 'fa fa-circle-o', '2018-04-18', 'users', 9, 'User_model', 0, 'active'),
(11, 'Hak Akses', 'lorem ipsum dolor sit amet', 'roles', 'fa fa-circle-o', '2018-04-18', 'roles', 9, 'Role_model', 0, 'active'),
(12, 'Module', 'lorem ipsum dolor sit amet', 'modules', 'fa fa-circle-o', '2018-04-18', 'modules', 9, 'Module_model', 0, 'active'),
(13, 'Fasilitas', 'Master data fasilitas', 'mdfacilities', 'fa fa-circle-o', '2020-07-02', 'mdfacilities', 9, 'Mdfacility_model', 0, 'active'),
(14, 'Sistem Penyewaan', 'Untuk diklat', '#', 'fa fa-home', '2020-07-02', 'open_data_system', NULL, '-', 0, 'active'),
(16, 'Diklat', 'Untuk diklat', 'trainings', 'fa fa-circle-o', '2020-07-02', 'trainings', 14, 'Mdtraining_model', 2, 'active'),
(17, 'Transaksi', 'Untuk transaksi', '#', 'fa fa-money', '2020-07-03', 'open_data_sales', NULL, '-', 0, 'active'),
(18, 'Faktur', '', 'invoices', 'fa fa-circle-o', '2020-07-03', 'invoices', 17, 'Invoice_model', 0, 'active'),
(19, 'Kwitansi', 'Untuk kwitansi', 'receipts', 'fa fa-circle-o', '2020-07-03', 'receipts', 17, 'Receipt_model', 0, 'active'),
(20, 'Akademik', '', '#', 'fa fa-book', '2020-07-03', 'open_data_administration', NULL, '-', 0, 'active'),
(21, 'Mahasiswa', 'untuk mahasiswa', 'students', 'fa fa-circle-o', '2020-07-03', 'students', 20, 'Student_model', 0, 'active'),
(22, 'Jadwal', 'untuk jadwal', 'schedules', 'fa fa-circle-o', '2020-07-03', 'schedules', 20, 'Schedule_model', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `module_applications`
--

CREATE TABLE `module_applications` (
  `id` int(5) NOT NULL,
  `module_id` int(5) NOT NULL,
  `action_read` varchar(30) NOT NULL,
  `action_create` varchar(30) NOT NULL,
  `action_update` varchar(30) NOT NULL,
  `action_destroy` varchar(30) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_applications`
--

INSERT INTO `module_applications` (`id`, `module_id`, `action_read`, `action_create`, `action_update`, `action_destroy`, `created_at`) VALUES
(3, 9, 'can :read, ', 'can :create, ', 'can :update, ', 'can :destroy, ', '0000-00-00'),
(4, 10, 'can :read, User_model', 'can :create, User_model', 'can :update, User_model', 'can :destroy, User_model', '0000-00-00'),
(5, 11, 'can :read, Role_model', 'can :create, Role_model', 'can :update, Role_model', 'can :destroy, Role_model', '0000-00-00'),
(6, 12, 'can :read, Module_model', 'can :create, Module_model', 'can :update, Module_model', 'can :destroy, Module_model', '0000-00-00'),
(7, 13, 'can :read, Mdfacility_model', 'can :create, Mdfacility_model', 'can :update, Mdfacility_model', 'can :destroy, Mdfacility_model', '0000-00-00'),
(8, 14, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(10, 16, 'can :read, Facility_model', 'can :create, Facility_model', 'can :update, Facility_model', 'can :destroy, Facility_model', '0000-00-00'),
(11, 17, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(12, 18, 'can :read, Invoice_model', 'can :create, Invoice_model', 'can :update, Invoice_model', 'can :destroy, Invoice_model', '0000-00-00'),
(13, 19, 'can :read, Receipt_model', 'can :create, Receipt_model', 'can :update, Receipt_model', 'can :destroy, Receipt_model', '0000-00-00'),
(14, 20, 'can :read, -', 'can :create, -', 'can :update, -', 'can :destroy, -', '0000-00-00'),
(15, 21, 'can :read, Student_model', 'can :create, Student_model', 'can :update, Student_model', 'can :destroy, Student_model', '0000-00-00'),
(16, 22, 'can :read, Schedule_model', 'can :create, Schedule_model', 'can :update, Schedule_model', 'can :destroy, Schedule_model', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `responsibility` varchar(30) NOT NULL,
  `nip` int(20) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'pending',
  `receipt_no` varchar(12) NOT NULL,
  `recomendation_no` varchar(12) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `delete`, `created_at`) VALUES
(1, 'Superadmin', 'superadmin', 'active', '2018-04-16'),
(2, 'Admin', 'lorem', 'deleted', '2018-04-16'),
(3, 'Import Master', 'loremasita', 'deleted', '2018-04-16');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `module_application_id` int(5) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 1,
  `is_add` tinyint(1) NOT NULL DEFAULT 1,
  `is_update` tinyint(1) NOT NULL DEFAULT 1,
  `is_delete` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_application_id`, `is_read`, `is_add`, `is_update`, `is_delete`, `created_at`) VALUES
(2, 1, 3, 1, 1, 1, 1, '0000-00-00'),
(3, 1, 4, 1, 0, 1, 0, '0000-00-00'),
(4, 1, 5, 1, 1, 1, 0, '0000-00-00'),
(5, 1, 6, 1, 1, 1, 0, '0000-00-00'),
(6, 1, 7, 1, 1, 1, 1, '0000-00-00'),
(7, 1, 8, 1, 1, 1, 1, '0000-00-00'),
(9, 1, 10, 1, 1, 1, 1, '0000-00-00'),
(10, 1, 11, 1, 1, 1, 1, '0000-00-00'),
(11, 1, 12, 1, 1, 1, 1, '0000-00-00'),
(12, 1, 13, 1, 1, 1, 1, '0000-00-00'),
(13, 1, 14, 1, 1, 1, 1, '0000-00-00'),
(14, 1, 15, 1, 1, 1, 1, '0000-00-00'),
(15, 1, 16, 1, 1, 1, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `major` varchar(20) NOT NULL,
  `semester` varchar(5) NOT NULL,
  `training_specification` text NOT NULL,
  `accreditation` varchar(5) NOT NULL DEFAULT 'A',
  `no_mou` varchar(20) NOT NULL,
  `date_cooperation_start` date NOT NULL,
  `date_cooperation_end` date NOT NULL,
  `address` text NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `training_date_start` date NOT NULL,
  `training_date_end` date NOT NULL,
  `participants` int(20) NOT NULL,
  `approve_date` date NOT NULL,
  `mentor` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'pending',
  `file_proposal` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `name`, `major`, `semester`, `training_specification`, `accreditation`, `no_mou`, `date_cooperation_start`, `date_cooperation_end`, `address`, `contact_person`, `phone`, `email`, `training_date_start`, `training_date_end`, `participants`, `approve_date`, `mentor`, `status`, `file_proposal`, `user_id`, `created_at`, `delete`) VALUES
(2, 'Diklat 1', 'Psikologi', 'S2', 'Praktek Klinik Jiwa D3', 'B', '001', '2020-07-01', '2020-07-30', 'lorem ipsum', 'Hasan A.F', '0019', 'alfarisisholehh@yahoo.com', '2020-07-01', '2020-07-10', 100, '0000-00-00', 'Hasan', 'pending', '', 4, '2020-07-02', 'active'),
(3, 'Diklat 2', 'Psikologi', 'D3', 'ProgramProfesi NERS', '', '002', '2020-07-01', '2020-07-30', 'lorem', 'Hasan A.F', '0019', 'alfarisisholehh@yahoo.com', '2020-07-02', '2020-07-15', 151, '0000-00-00', 'Hasan', 'approve', '', 4, '2020-07-02', 'active'),
(4, 'Diklat 3', 'Pekerja Sosial', 'D2', 'Profesi Farmasi', 'C', '123456', '2017-07-14', '2017-07-15', 'Jl. Letkol G.A Manulang No.214\r\n214', '40553', '0895328679873', 'sadrclothes@gmail.com', '2017-07-15', '2017-07-15', 100, '0000-00-00', 'HASAN', 'approve', '', 4, '2020-07-03', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `training_facilities`
--

CREATE TABLE `training_facilities` (
  `id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `frequency` int(5) NOT NULL,
  `created_at` int(11) NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `training_facilities`
--

INSERT INTO `training_facilities` (`id`, `training_id`, `facility_id`, `price`, `frequency`, `created_at`, `delete`) VALUES
(1, 3, 2, 250000, 10, 2020, 'active'),
(2, 3, 1, 10200000, 12, 2020, 'active'),
(5, 2, 2, 250000, 10, 2020, 'active'),
(6, 4, 2, 500000, 20, 2020, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role_id` int(5) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `role_id`, `created_at`, `delete`) VALUES
(1, 'Loreal', 'andoyoandoyo@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2018-04-16', 'deleted'),
(2, 'Lorem ipsum dolor Hasan', 'dolor@lorem.com', 'lorem_ipsum', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2018-04-03', 'deleted'),
(3, 'Hasan Al Farisi', 'learn@kiranatama.com', 'lorem', 'd2e16e6ef52a45b7468f1da56bba1953', 1, '2018-04-15', 'deleted'),
(4, 'Hasan Al Farisi', 'learn@gmail.com', 'admin', '25d55ad283aa400af464c76d713c07ad', 1, '2018-04-16', 'active'),
(5, 'role_percobaan', 'role@role.com', 'rolade', 'a516ff74e222c3bbd3ab924e96fefe0c', 1, '2018-04-19', 'deleted');

-- --------------------------------------------------------

--
-- Structure for view `get_roles_access`
--
DROP TABLE IF EXISTS `get_roles_access`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_roles_access`  AS  select `role_modules`.`id` AS `id`,`role_modules`.`role_id` AS `role_id`,`role_modules`.`module_application_id` AS `module_application_id`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete`,`users`.`id` AS `user_id`,`modules`.`path_module` AS `path`,`modules`.`id` AS `module_id` from ((((`role_modules` join `roles` on(`roles`.`id` = `role_modules`.`role_id`)) join `users` on(`users`.`role_id` = `roles`.`id`)) join `module_applications` on(`module_applications`.`id` = `role_modules`.`module_application_id`)) join `modules` on(`modules`.`id` = `module_applications`.`module_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `get_role_modules`
--
DROP TABLE IF EXISTS `get_role_modules`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_role_modules`  AS  select `roles`.`id` AS `role_id`,`role_modules`.`id` AS `role_module_id`,`module_applications`.`id` AS `module_application_id`,`modules`.`id` AS `module_id`,`modules`.`name` AS `module_name`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete` from (((`role_modules` join `module_applications` on(`module_applications`.`id` = `role_modules`.`module_application_id`)) join `modules` on(`modules`.`id` = `module_applications`.`module_id`)) join `roles` on(`roles`.`id` = `role_modules`.`role_id`)) where `modules`.`child` is not null ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdfacilities`
--
ALTER TABLE `mdfacilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_applications`
--
ALTER TABLE `module_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_facilities`
--
ALTER TABLE `training_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mdfacilities`
--
ALTER TABLE `mdfacilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `module_applications`
--
ALTER TABLE `module_applications`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `training_facilities`
--
ALTER TABLE `training_facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
