<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('medical_values/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    	<th width="3%">No</th>
						<th width="15%">NIM</th>
						<th width="20%">Nama mahasiswa</th>
						<th width="20%">Instansi</th>
						<th width="20%">Jurusan</th>
						<th width="10%">CSS1</th>
						<th width="10%">CSS2</th>
						<th width="10%">BST1</th>
						<th width="10%">BST2</th>
						<th width="10%">BST3</th>
						<th width="10%">BST4</th>
						<th width="10%">BST0</th>
						<th width="10%">BST6</th>
						<th width="10%">CRS1</th>
						<th width="10%">BST2</th>
						<th width="10%">Minicex</th>
						<th width="10%">Ujian Akhir</th>
						<th width="10%">Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($medical_values) == 0 || $medical_values->num_rows() == null){
	                            echo"<td colspan='18'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($medical_values->result() as $medical_value) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $medical_value->nim ?></td>
								<td><?php echo $medical_value->name ?></td>
								<td><?php echo $medical_value->instance ?></td>
								<td><?php echo $medical_value->majors ?></td>
								<td><?php echo $medical_value->css1 ?></td>
								<td><?php echo $medical_value->css2 ?></td>
								<td><?php echo $medical_value->bst1 ?></td>
								<td><?php echo $medical_value->bst2 ?></td>
								<td><?php echo $medical_value->bst3 ?></td>
								<td><?php echo $medical_value->bst4 ?></td>
								<td><?php echo $medical_value->bst5 ?></td>
								<td><?php echo $medical_value->bst6 ?></td>
								<td><?php echo $medical_value->crs1 ?></td>
								<td><?php echo $medical_value->crs2 ?></td>
								<td><?php echo $medical_value->minicex ?></td>
								<td><?php echo $medical_value->final_exam ?></td>
								<td><?php echo $medical_value->description?></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $medical_values_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $medical_values_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>