<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Pembimbing</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $schedule->mentor_name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Program Study</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $schedule->program_study ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama ruang</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $schedule->room_name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Deskripsi</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $schedule->description ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>schedules" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>schedules/edit/<?php echo $schedule->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>