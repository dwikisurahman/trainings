<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('schedules/create'); ?>
<?php }else{ ?>
	<?php echo form_open('schedules/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="pull-left">Nama Pembimbing<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input type="text" name="mentor_name" class="form-control" value="<?php echo isset($value['mentor_name']) ? $value['mentor_name'] : null ?>">
						<input type="hidden" class="form-control" name="id" value="<?php echo isset($schedule) ? $schedule->id : isset($value['id']) ? $value['id'] : null; ?>">
					</div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Program study<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-3">
							<input type="text" name="program_study" class="form-control" value="<?php echo isset($value['program_study']) ? $value['program_study'] : null ?>">
						</div>
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="pull-left">Jadwal<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<select name='mdroom_id' class="form-control">
                            <option value=''>- Pilih Ruangan -</option>
                            <option value='1' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '1' ? 'selected' : "" : "" ?>>Nuri</option>
                            <option value='2' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '2' ? 'selected' : "" : "" ?>>Elang</option>
                            <option value='3' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '3' ? 'selected' : "" : "" ?>>Rajawali</option>
                            <option value='4' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '4' ? 'selected' : "" : "" ?>>Garuda</option>
                            <option value='5' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '5' ? 'selected' : "" : "" ?>>Keswara</option>
                            <option value='6' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '6' ? 'selected' : "" : "" ?>>Gelatik</option>
                            <option value='7' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '7' ? 'selected' : "" : "" ?>>Cendrawasih</option>
                            <option value='8' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '8' ? 'selected' : "" : "" ?>>Perkutut</option>
                            <option value='9' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == 'Merak' ? 'selected' : "" : "" ?>>Merak</option>
                            <option value='10' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == '10' ? 'selected' : "" : "" ?>>Merpati</option>
                            <option value='Pendidikan Lainnya' <?php echo isset($value['mdroom_id']) ? $value['mdroom_id'] == 'Pendidikan Lainnya' ? 'selected' : "" : "" ?>>Pendidikan Lainnya</option>
                        </select>
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="pull-left">Deskripsi</label>
					</div>
					<div class="controls col-lg-8">
						<textarea name="description" value="<?php echo isset($value['description']) ? $value['description'] : null ?>" class="form-control"><?php echo isset($value['description']) ? $value['description'] : null ?></textarea>
					</div>
				</div>				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>schedules" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>