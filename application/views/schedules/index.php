<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('schedules/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    	<th width="3%">No</th>
						<th width="15%">Nama Pembinmbing</th>
						<th width="15%">Nama Ruangan</th>
						<th width="15%">Bimbingan Program Study</th>
						<th width="20%">Deskripsi</th>
						<th width="15%">Tgl Buat</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($schedules) == 0 || $schedules->num_rows() == null){
	                            echo"<td colspan='8'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($schedules->result() as $schedule) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $schedule->mentor_name ?></td>
								<td><?php echo $schedule->room_name ?></td>
								<td><?php echo $schedule->program_study ?></td>
								<td><?php echo $schedule->description ?></td>
								<td><?php echo date("d F Y", strtotime($schedule->created_at)) ?></td>
								<td>
									<a href="<?= base_url(); ?>schedules/show/<?php echo $schedule->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>schedules/edit/<?php echo $schedule->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>schedules/destroy/<?php echo $schedule->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $schedules_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $schedules_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>