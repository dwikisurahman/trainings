<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Example</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $nursing_value->name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Deskripsi</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $nursing_value->description ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>nursing_values" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>nursing_values/edit/<?php echo $nursing_value->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>