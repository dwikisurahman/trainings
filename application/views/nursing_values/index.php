<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('nursing_values/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    	<th width="3%">No</th>
					    	<th width="15%">NIM</th>
							<th width="20%">Nama mahasiswa</th>
							<th width="20%">Instansi</th>
							<th width="15%">Jurusan</th>
							<th width="15%">IP</th>
							<th width="15%">SPTK</th>
							<th width="15%">Prepost</th>
							<th width="15%">Dokep</th>
							<th width="15%">TAK</th>
							<th width="15%">Penyuluhan</th>
							<th width="15%">Prestasi</th>
							<th width="15%">Sikap</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($nursing_values) == 0 || $nursing_values->num_rows() == null){
	                            echo"<td colspan='14'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($nursing_values->result() as $example) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $example->nim ?></td>
								<td><?php echo $example->name ?></td>
								<td><?php echo $example->instance ?></td>
								<td><?php echo $example->majors ?></td>
								<td><?php echo $example->ip ?></td>
								<td><?php echo $example->sptk ?></td>
								<td><?php echo $example->prepost ?></td>
								<td><?php echo $example->dokep ?></td>
								<td><?php echo $example->tak ?></td>
								<td><?php echo $example->counseling ?></td>
								<td><?php echo $example->achievement ?></td>
								<td><?php echo $example->attitude ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $Nursing_values_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $Nursing_values_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>