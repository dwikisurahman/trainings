<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Diklat</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $invoice->training_name ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Invoice nomor</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $invoice->invoice_no ?>
				</div>
			</div>
			<div class="clearfix"></div><br>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Total harga</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $invoice->total_amount ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Status pembayaran</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $invoice->status ?>
				</div>
			</div>
			<div class="clearfix"></div><br>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Bukti bayar</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $invoice->file_upload ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Mengajukan</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $invoice->user_name ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>invoices" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>invoices/edit/<?php echo $invoice->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>