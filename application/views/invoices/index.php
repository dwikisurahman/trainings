<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('invoices/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    <th width="3%">No</th>
						<th width="15%">Nama Diklat</th>
						<th width="8%">No. Invoice</th>
						<th width="15%">Total Harga</th>
						<th width="15%">Status</th>
						<th width="15%">Mengajukan</th>
						<th width="10%">Tgl buat</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($invoices) == 0 || $invoices->num_rows() == null){
	                            echo"<td colspan='8'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($invoices->result() as $example) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $example->training_name ?></td>
								<td><?php echo $example->invoice_no ?></td>
								<td>Rp.<?php echo number_format($example->total_amount) ?></td>
								<td><?php echo $example->status == 'pending' ? '<a class="btn btn-sm btn-warning">pending</a>' : '<a class="btn btn-info">'.$example->status.'</a>' ?></td>
								<td><?php echo $example->user_name ?></td>
								<td><?php echo date("d F Y", strtotime($example->created_at)) ?></td>
								<td>
									<a href="<?= base_url(); ?>invoices/show/<?php echo $example->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>invoices/edit/<?php echo $example->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>invoices/vprint/<?php echo $example->id ?>" class="btn btn-warning btn-sm" title="Print"><i class="fa fa-print"></i></a>
									<a href="<?= base_url(); ?>invoices/destroy/<?php echo $example->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $invoices_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $invoices_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>