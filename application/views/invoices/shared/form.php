<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open_multipart('invoices/create'); ?>
<?php }else{ ?>
	<?php echo form_open_multipart('invoices/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">No Invoice<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input type="text" name="invoice_no" class="form-control" value="<?php echo isset($value['invoice_no']) ? $value['invoice_no'] : null ?>" readonly>
						<input type="hidden" class="form-control" name="id" value="<?php echo isset($example) ? $example->id : isset($value['id']) ? $value['id'] : null; ?>">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-3">
							<label class="pull-left">Upload Bukti Bayar</label>
						</div>
						<div class="controls col-lg-5">
							<input name="file_upload" id="file_upload" class="form-control" type="file" accept="image/png, image/gif, image/jpeg, image/jpg, image/pdf">
						</div>					
					</div>	
					<?php if ($users->role_id == 4){?>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-3">
							<label class="pull-left">Status<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-3">
							<select name='status' class="form-control">
	                            <option value=''>- Pilih Status -</option>
	                            <option value='pending' <?php echo isset($value['status']) ? $value['status'] == 'pending' ? 'selected' : "" : "" ?>>Pending</option>
	                            <option value='submit' <?php echo isset($value['status']) ? $value['status'] == 'submit' ? 'selected' : "" : "" ?>>Ajukan</option>
	                            <option value='approve' <?php echo isset($value['status']) ? $value['status'] == 'approve' ? 'selected' : "" : "" ?>>Diterima</option>
	                        </select>
						</div>
					</div>
					<?php } ?>			
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>invoices" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>