<div class="row">
	<div class="col-sm-12">
		<div class="card-box">

		<body>
		<table class='tbl_header'>
		  <tr>
		    <td rowspan='6' style='width:160px; text-align:center; '><img src="<?= base_url(); ?>assets/images/lp.png" width='100' height='111'/></td>
		    <td style='width:1247px; text-align:center;   font-family:Arial, Helvetica, sans-serif; '>PEMERINTAH PROVINSI JAWA BARAT</td>
		  </tr>
		  <tr>
		    <td style=' text-align:center;  font-family:Arial, Helvetica, sans-serif; '><font size='3' ><b>RUMAH SAKIT JIWA</b></font></td>
		  </tr>
		  <tr>
		    <td align='center'>Jalan Kolonel Masturi KM. 7</td>
		  </tr>
		  <tr>
		    <td align='center'>Telepon: (022) 2700260 Faksimil: (022) 2700304</td>
		  </tr>
		  <tr>
		    <td align='center'>Website: www.rsj.jabarprov.go.id email: rsj@jabarprov.go.id</td>
		  </tr>
		  <tr>
		    <td align='center'>KABUPATEN BANDUNG BARAT - 40551</td>
		  </tr>
		  <tr>
		    <td colspan='2' align='center'><hr width='100%' size='2' color='#000000'></td>
		  </tr>
		</table>

		<h3 align="center" style="margin-top: 5px; font-size: 25px; font-weight: bold;">
		  <b><U>INVOICE</U></b>
		</h3>

		<table class="invoice-logo" cellspacing="0" style="margin-top: 5px; font-weight: 80;">
		  <tr>
		    <th width="50px;" align="border-right"></th>
		    <th width="1px;" align="border-center"></th>
		    <th width="200px" align="border-right"></th>
		  </tr>
		  <tr>
		    <th align="left">No. Invoice</th>
		    <th align="center">:</th>
		    <td align="left"><?php echo $invoice->invoice_no ?></td>
		  </tr>
		  <tr>
		    <th align="left">Nama Diklat</th>
		    <th align="center">:</th>
		    <td align="left"><?php echo $invoice->training_name ?></td>
		  </tr>
		  <tr>
		    <th align="left">Total Biaya</th>
		    <th align="center">:</th>
		    <td align="left">Rp. <?php echo number_format($invoice->total_amount) ?>,-</td>
		  </tr>
		</table><br>
		<table  class="invoice-logo" cellspacing="1">
		  <tr>
		    <th width="100px;" align="border-left"></th>
		    <th width="200px;" align="border-left"></th>
		    <th width="400px;" align="border-left"></th>
		    <th width="300px;" align="border-right"></th>
		  </tr>
		  <tr>
		   	<th colspan="2">Cimahi <?php echo date("d F Y", strtotime($invoice->created_at)) ?></th>
		  	<td></td>
		    <td></td>
		    <td></td>
		  </tr>
		  <tr>
		  	<th></th>
		  	<th></th>
		  	<td></td>
		  	<th align="center">Penerima</th>
		  </tr>
		</table><br><br>
		<table class="bordered">
		 	<tr>
		    <th width="100px;" align="border-left"></th>
		    <th width="200px;" align="border-left"></th>
		    <th width="400px;" align="border-left"></th>
		    <th width="300px;" align="border-right"></th>
		  </tr>
			<tr>
		  		<th></th>
		  		<td></td>
		  		<td></td>
		  		<th align="center">( <?php echo $invoice->user_name ?> )</th>
		  	</tr>
		  	<tr>
		  		<th></th>
		  		<td></td>
		  		<td></td>
		  		<th align="center">Nama Jelas</th>
		  	</tr>
		</table>
		</body>

		<script>
		    window.print();
		  </script>
		</div>
	</div>
</div>