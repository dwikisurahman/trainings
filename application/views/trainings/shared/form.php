<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('trainings/create'); ?>
<?php }else{ ?>
	<?php echo form_open('trainings/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Nama Instansi<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input type="text" name="name" class="form-control" value="<?php echo isset($value['name']) ? $value['name'] : null ?>">
						<input type="hidden" class="form-control" name="id" value="<?php echo isset($example) ? $example->id : isset($value['id']) ? $value['id'] : null; ?>">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">No MOU<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input type="text" name="no_mou" class="form-control" value="<?php echo isset($value['no_mou']) ? $value['no_mou'] : null ?>">						
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Mentor<span class="required">*</span></label>
					</div>				
					<div class="controls col-lg-3">
						<input type="text" name="mentor" class="form-control" value="<?php echo isset($value['mentor']) ? $value['mentor'] : null ?>">						
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Jurusan<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<select name='major' class="form-control">
                            <option value=''>- Pilih Jurusan -</option>
                            <option value='Kedokteran' <?php echo isset($value['major']) ? $value['major'] == 'Kedokteran' ? 'selected' : "" : "" ?>>Kedokteran</option>
                            <option value='Keperawatan' <?php echo isset($value['major']) ? $value['major'] == 'Keperawatan' ? 'selected' : "" : "" ?>>Keperawatan</option>
                            <option value='Psikologi' <?php echo isset($value['major']) ? $value['major'] == 'Psikologi' ? 'selected' : "" : "" ?>>Psikologi</option>
                            <option value='Farmasi' <?php echo isset($value['major']) ? $value['major'] == 'Farmasi' ? 'selected' : "" : "" ?>>Farmasi</option>
                            <option value='Pekerja Sosial' <?php echo isset($value['major']) ? $value['major'] == 'Pekerja Sosial' ? 'selected' : "" : "" ?>>Pekerja Sosial</option>
                            <option value='Psikologi' <?php echo isset($value['major']) ? $value['major'] == 'Psikologi' ? 'selected' : "" : "" ?>>Psikologi</option>
                            <option value='Akutansi' <?php echo isset($value['major']) ? $value['major'] == 'Akutansi' ? 'selected' : "" : "" ?>>Akutansi</option>
                            <option value='Rekam Medis' <?php echo isset($value['major']) ? $value['major'] == 'Rekam Medis' ? 'selected' : "" : "" ?>>Rekam Medis</option>
                            <option value='Pendidikan Lainnya' <?php echo isset($value['major']) ? $value['major'] == 'Pendidikan Lainnya' ? 'selected' : "" : "" ?>>Pendidikan Lainnya</option>
                        </select>
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Jenjang<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<select name='semester' class="form-control">
                            <option value=''>- Pilih Jenjang -</option>
                            <option value='SMA' <?php echo isset($value['semester']) ? $value['semester'] == 'SMA' ? 'selected' : "" : "" ?>>SMA</option>
                            <option value='SMK' <?php echo isset($value['semester']) ? $value['semester'] == 'SMK' ? 'selected' : "" : "" ?> <?php echo isset($value['semester']) ? $value['semester'] == 'D1' ? 'selected' : "" : "" ?>>SMK</option>
                            <option value='D1'>D1</option>
                            <option value='D2' <?php echo isset($value['semester']) ? $value['semester'] == 'D2' ? 'selected' : "" : "" ?>>D2</option>
                            <option value='D3' <?php echo isset($value['semester']) ? $value['semester'] == 'D3' ? 'selected' : "" : "" ?>>D3</option>
                            <option value='D4' <?php echo isset($value['semester']) ? $value['semester'] == 'D4' ? 'selected' : "" : "" ?>>D4</option>
                            <option value='S1' <?php echo isset($value['semester']) ? $value['semester'] == 'S1' ? 'selected' : "" : "" ?>>S1</option>
                            <option value='S2' <?php echo isset($value['semester']) ? $value['semester'] == 'S2' ? 'selected' : "" : "" ?>>S2</option>
                        </select>
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<label class="col-sm-3 control-label">Spesifikasi</label>
                    <div class="col-sm-3">
                        <select name='training_specification' class="form-control">
                            <option value=''>- Spesifikasi Diklit-</option>
                            <option value='Pendidikan Sarjana Kedokteran' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Pendidikan Sarjana Kedokteran' ? 'selected' : "" : "" ?>>Pendidikan Sarjana Kedokteran</option>
                            <option value='Pendidikan Dokter Spesialis' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Pendidikan Dokter Spesialis' ? 'selected' : "" : "" ?>>Pendidikan Dokter Spesialis</option>
                            <option value='Praktek Klinik Jiwa D3' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Praktek Klinik Jiwa D3' ? 'selected' : "" : "" ?>>Praktek Klinik Jiwa D3</option>
                            <option value='ProgramProfesi NERS' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'ProgramProfesi NERS' ? 'selected' : "" : "" ?>>ProgramProfesi NERS</option>
                            <option value='Praktek Belajar Lapangan SI' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Praktek Belajar Lapangan SI' ? 'selected' : "" : "" ?>>Praktek Belajar Lapangan SI</option>
                            <option value='Profesi Farmasi' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Profesi Farmasi' ? 'selected' : "" : "" ?>>Profesi Farmasi</option>
                            <option value='Praktek Kerja Lapangan' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Praktek Kerja Lapangan' ? 'selected' : "" : "" ?>>Praktek Kerja Lapangan</option>
                            <option value='Penelitian' <?php echo isset($value['training_specification']) ? $value['training_specification'] == 'Penelitian' ? 'selected' : "" : "" ?>>Penelitian</option>
                        </select>                        
                    </div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Akreditasi<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">						                        
                        <select name='accreditation' class="form-control">
                            <option value=''>- Pilih AKreditasi-</option>
                            <option value='A' <?php echo isset($value['accreditation']) ? $value['accreditation'] == 'A' ? 'selected' : "" : "" ?>>A</option>
                            <option value='B' <?php echo isset($value['accreditation']) ? $value['accreditation'] == 'B' ? 'selected' : "" : "" ?>>B</option>
                            <option value='C' <?php echo isset($value['accreditation']) ? $value['accreditation'] == 'C' ? 'selected' : "" : "" ?>>C</option>
                            <option value='BAN-PT' <?php echo isset($value['accreditation']) ? $value['accreditation'] == 'BAN-PT' ? 'selected' : "" : "" ?>>BAN-PT</option>
                        </select>
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Nama CP<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input type="text" name="contact_person" class="form-control" value="<?php echo isset($value['contact_person']) ? $value['contact_person'] : null ?>">						
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Nomor CP<span class="required">*</span></label>
					</div>				
					<div class="controls col-lg-3">
						<input type="text" name="phone" class="form-control" value="<?php echo isset($value['phone']) ? $value['phone'] : null ?>">						
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Email CP<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input type="text" name="email" class="form-control" value="<?php echo isset($value['email']) ? $value['email'] : null ?>">						
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<?php 
					if ($users->role_id == 1){
				?>
				<div class="control-group">
					
					<div class="control-label col-lg-3">
						<label class="pull-left">Tanggal mulai kerjasama<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<div class="input-group date">
	                        <div class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </div>
                            <input type="date" name="date_cooperation_start" class="form-control" value="<?php echo isset($value['date_cooperation_start']) ? $value['date_cooperation_start'] : null ?>">						                        
	                    </div>				
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Tanggal berakhir kerjasama<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<div class="input-group date">
	                        <div class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </div>
                            <input type="date" name="date_cooperation_end" class="form-control" value="<?php echo isset($value['date_cooperation_end']) ? $value['date_cooperation_end'] : null ?>">               
	                    </div>					
					</div>
										
				</div>
			<?php } ?>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Tanggal mulai diklat<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<div class="input-group date">
	                        <div class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </div>
                            <input type="date" name="training_date_start" class="form-control" value="<?php echo isset($value['training_date_start']) ? $value['training_date_start'] : null ?>">						                        
	                    </div>				
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Tanggal berakhir diklat<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<div class="input-group date">
	                        <div class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </div>
                            <input type="date" name="training_date_end" class="form-control" value="<?php echo isset($value['training_date_end']) ? $value['training_date_end'] : null ?>">               
	                    </div>					
					</div>					
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Jumlah peserta<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input type="text" name="participants" class="form-control" value="<?php echo isset($value['participants']) ? $value['participants'] : null ?>">						
					</div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Upload proposal<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<input name="file_proposal" id="file_proposal" class="form-control" type="file" accept="image/pdf">				
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Alamat instansi</label>
					</div>
					<div class="controls col-lg-8">
						<textarea name="address" value="<?php echo isset($value['address']) ? $value['address'] : null ?>" class="form-control"><?php echo isset($value['address']) ? $value['address'] : null ?></textarea>
					</div>
				</div>											
				<div class="clearfix"><br><br><br><br><br></div>
				<?php 
				if ($users->role_id == 4){?>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Status<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-3">
						<select name='status' class="form-control">
                            <option value=''>- Pilih Status -</option>
                            <option value='pending' <?php echo isset($value['status']) ? $value['status'] == 'pending' ? 'selected' : "" : "" ?>>Pending</option>
                            <option value='submit' <?php echo isset($value['status']) ? $value['status'] == 'submit' ? 'selected' : "" : "" ?>>Ajukan</option>
                            <option value='approve' <?php echo isset($value['status']) ? $value['status'] == 'approve' ? 'selected' : "" : "" ?>>Diterima</option>
                        </select>
					</div>
				</div>
				<?php } ?>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>trainings" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>