<?php $this->load->helper('training'); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('trainings/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    <th width="3%">No</th>
						<th width="15%">ID Diklat</th>
						<th width="13%">ID Fasilitas</th>
						<th width="5%">Price</th>						
						<th width="5%">Frekuensi</th>
						<th width="10%">Tgl Buat</th>
						<th width="16%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($training_facilities) == 0 || $training_facilities->num_rows() == null){
	                            echo"<td colspan='7'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($training_facilities->result() as $example) { ?>
							<tr>								
								<td><?php echo $no++ ?></td>
								<td><?php echo $example->training_id ?></td>
								<td><?php echo $example->facility_id ?></td>
								<td class="text-right">
									Rp.<?php echo number_format(check_training_faility($example->id)->result()[0]->sum_price) ?>
								</td>
								<td><?php echo $example->frequency ?></td>
								<td>
									<a href="<?= base_url(); ?>trainings/show/<?php echo $example->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>trainings/edit/<?php echo $example->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>trainings/destroy/<?php echo $example->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $trainings_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $trainings_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>