<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create_facility' || $this->uri->segment(2) == 'add_facility') { ?>
	<?php echo form_open('trainings/create_facility'); ?>
<?php }else{ ?>
	<?php echo form_open('trainings/update_facility'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<input type="hidden" name="training_id" value="<?= $_GET['training_id'] ?>">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
					<thead>
					    <tr role="row">
					    	<th width="5%">No</th>
					    	<th width="5%"></th>
					    	<th>Rincian</th>
					    	<th width="20%">Biaya</th>
					    	<th width="10%">Frek</th>
					    	<th width="15%">Satuan</th>
					    	<th width="15%">Jumlah</th>
					    </tr>
					</thead>
					<tbody>						
						<?php $no = 1; foreach ($mdfacilities->result() as $example) { ?>
							<tr>												
								<td><?php echo $no++ ?></td>								
								<input type="hidden" value="<?php echo isset($training_facilities[$no-2]) != 0 ? $training_facilities[$no-2]->id : "" ?>" name="id[<?php echo $example->id ?>]">
								<td><input type="checkbox" name="facility_id[<?php echo $example->id ?>]" value="<?php echo $example->id ?>" <?php echo isset($training_facilities[$no-2]) != 0 ? $training_facilities[$no-2]->facility_id == $example->id ? 'checked' : '' : "" ?>></td>
								<td><?php echo $example->name ?></td>
								<td>Rp.<?php echo number_format($example->price) ?>/<?php echo $example->perunit ?></td>
								<td><input type="text" name="frequency[<?php echo $example->id ?>]" class="form-control only-numeric" placeholder="0" value="<?php echo isset($training_facilities[$no-2]) != 0 ? $training_facilities[$no-2]->frequency : "" ?>"></td>
								<td><?php echo $example->perunit ?></td>
								<td class="text-right">Rp.<?php echo isset($training_facilities[$no-2]) != 0 ? $training_facilities[$no-2]->price : "" ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>trainings/" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>