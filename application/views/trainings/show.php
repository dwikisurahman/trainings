<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Nama Instansi</label>
				</div>
				<div class="controls col-lg-8">
					<?php echo $training->name ?>
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">No MOU</label>
				</div>
				<div class="controls col-lg-3">							
					<?php echo $training->no_mou ?>			
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Mentor</label>
				</div>				
				<div class="controls col-lg-3">
					<?php echo $training->mentor ?>								
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Jurusan</label>
				</div>
				<div class="controls col-lg-3">						
                    <?php echo $training->major ?>			
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Jenjang</label>
				</div>
				<div class="controls col-lg-3">						
                    <?php echo $training->semester ?>			
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<label class="col-sm-3 control-label">Spesifikasi</label>
                <div class="col-sm-3">
                    <?php echo $training->training_specification ?>      
                </div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Akreditasi</label>
				</div>
				<div class="controls col-lg-3">						                                                
                    <?php echo $training->accreditation ?>  
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Nama CP</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $training->contact_person ?>  
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Nomor CP</label>
				</div>				
				<div class="controls col-lg-3">
					<?php echo $training->phone ?> 	
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Email CP</label>
				</div>
				<div class="controls col-lg-3">							
					<?php echo $training->email ?> 				
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Tanggal mulai kerjasama</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $training->date_cooperation_start ?> 				    	                    
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Tanggal berakhir kerjasama</label>
				</div>
				<div class="controls col-lg-3">
                    <?php echo $training->date_cooperation_end ?> 				    	                    
				</div>					
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Tanggal mulai diklat</label>
				</div>
				<div class="controls col-lg-3">      
                    <?php echo $training->training_date_start ?> 					                       	                   
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Tanggal berakhir diklat</label>
				</div>
				<div class="controls col-lg-3">                              
                    <?php echo $training->training_date_end ?> 					                  	                    
				</div>					
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Jumlah peserta</label>
				</div>
				<div class="controls col-lg-3">						
					<?php echo $training->participants ?> 					                                					
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Upload proposal</label>
				</div>
				<div class="controls col-lg-3">						
				</div>
			</div>
			<div class="clearfix"><br><br><br></div>
			<div class="control-group">
				<div class="control-label col-lg-3">
					<label class="pull-left">Alamat instansi</label>
				</div>
				<div class="controls col-lg-8">
					<?php echo $training->address ?>
				</div>
			</div>											
			<div class="clearfix"></div>			
		</div>		
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>trainings" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>trainings/edit/<?php echo $training->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>