<?php $this->load->helper('training'); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('trainings/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    <th width="3%">No</th>
						<th width="15%">Nama Diklat</th>
						<th width="13%">Tanggal Diklat</th>
						<th width="5%">Peserta</th>						
						<th width="5%">Status</th>
						<th width="10%">Tgl Buat</th>
						<th width="13%">Fasilitas</th>
						<th width="13%">Harga total fasilitas</th>
						<th width="16%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($trainings) == 0 || $trainings->num_rows() == null){
	                            echo"<td colspan='8'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($trainings->result() as $example) { ?>
							<tr>								
								<td><?php echo $no++ ?></td>
								<td><?php echo $example->name ?></td>
								<td><?php echo $example->training_date_start ?> s/d <?php echo $example->training_date_end ?></td>
								<td><?php echo $example->participants ?></td>
								<td><?php echo $example->status == 'pending' ? '<a class="btn btn-sm btn-warning">pending</a>' : '<a class="btn btn-info">'.$example->status.'</a>' ?></td>
								<td><?php echo date("d F Y", strtotime($example->created_at)); ?></td>
								<td>
									<?php if(check_training_faility($example->id)->result()[0]->sum_price != 0){ ?>
										<a href="<?php echo base_url() ?>/trainings/edit_facility?training_id=<?php echo $example->id ?>">Fasilitas ditambahkan</a>
									<?php }else{ ?>
										<a href="<?php echo base_url() ?>/trainings/add_facility?training_id=<?php echo $example->id ?>">Tambah fasilitas</a>										
									<?php } ?>
								</td>
								<td class="text-right">
									Rp.<?php echo number_format(check_training_faility($example->id)->result()[0]->sum_price) ?>
								</td>
								<td>
									<a href="<?= base_url(); ?>trainings/show/<?php echo $example->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>trainings/edit/<?php echo $example->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>trainings/destroy/<?php echo $example->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
									<a href="<?= base_url(); ?>trainings/change/<?php echo $example->id ?>?approve=submit" class="btn btn-warning btn-sm" onclick="return    confirm('Anda akan mengubah data ini, anda yakin ?');" title="Hapus">Ajukan</a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $trainings_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $trainings_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>