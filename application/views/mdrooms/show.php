<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Ruangan</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $mdroom->room_name ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Kapasitas Ruangan</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $mdroom->room_capacity ?>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="">Deskripsi</label>
				</div>
				<div class="controls col-lg-2">
					<?php echo $mdroom->description ?>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>mdrooms" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>mdrooms/edit/<?php echo $mdroom->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>