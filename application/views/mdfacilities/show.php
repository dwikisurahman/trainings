<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Fasilitas</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $mdfacility->name ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Biaya</label>
				</div>
				<div class="controls col-lg-4">
					Rp.<?php echo $mdfacility->price ?>/<?php echo $mdfacility->perunit ?>
				</div>
				<div class="clearfix"></div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Deskripsi</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $mdfacility->description ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>mdfacilities" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>mdfacilities/edit/<?php echo $mdfacility->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>