<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('mdfacilities/create'); ?>
<?php }else{ ?>
	<?php echo form_open('mdfacilities/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">				
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Nama Fasilitas<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input type="text" name="name" class="form-control" value="<?php echo isset($value['name']) ? $value['name'] : null ?>">
						<input type="hidden" class="form-control" name="id" value="<?php echo isset($example) ? $example->id : isset($value['id']) ? $value['id'] : null; ?>">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Biaya (Rp)<span class="required">*</span></label>
					</div>

					<div class="controls col-lg-2">												
						<input type="text" name="price" class="form-control only-numeric" value="<?php echo isset($value['price']) ? $value['price'] : null ?>" placeholder="0">
					</div>
					<div class="control-label col-lg-3">
						<label class="pull-left">Per unit biaya<span class="required">*</span></label>
					</div>

					<div class="controls col-lg-3">												
						<input type="text" name="perunit" class="form-control" value="<?php echo isset($value['perunit']) ? $value['perunit'] : null ?>" placeholder="pemakaian">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Deskripsi</label>
					</div>
					<div class="controls col-lg-8">
						<textarea name="description" value="<?php echo isset($value['description']) ? $value['description'] : null ?>" class="form-control"><?php echo isset($value['description']) ? $value['description'] : null ?></textarea>
					</div>
				</div>				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>mdfacilities" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>