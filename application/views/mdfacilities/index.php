<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('mdfacilities/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    <th width="3%">No</th>
						<th width="15%">Nama Fasilitas</th>
						<th width="20%">Biaya</th>
						<th width="15%">Tgl buat</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($mdfacilities) == 0 || $mdfacilities->num_rows() == null){
	                            echo"<td colspan='5'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($mdfacilities->result() as $object) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $object->name ?></td>
								<td>Rp.<?php echo $object->price ?>/<?php echo $object->perunit ?></td>
								<td><?php echo date("d F Y", strtotime($object->created_at)) ?></td>
								<td>
									<a href="<?= base_url(); ?>mdfacilities/show/<?php echo $object->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>mdfacilities/edit/<?php echo $object->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>mdfacilities/destroy/<?php echo $object->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $mdfacilities_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $mdfacilities_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>