<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('users/create'); ?>
<?php }else{ ?>
	<?php echo form_open('users/update'); ?>
<?php }?>
<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="col-sm-12">
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Nama Lengkap<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-8">
							<input type="text" name="name" class="form-control" value="<?php echo isset($value['name']) ? $value['name'] : null ?>">
							<input type="hidden" class="form-control" name="id" value="<?php echo isset($user) ? $user->id : isset($value['id']) ? $value['id'] : null; ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Email<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-8">
							<input type="email" name="email" class="form-control" value="<?php echo isset($value['email']) ? $value['email'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Username<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-8">
							<input type="text" name="username" class="form-control" value="<?php echo isset($value['username']) ? $value['username'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Posisi<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-4">
							<select name="role_id" class="form-control">
								<?php foreach ($roles->result() as $role){ ?>
									<option value="<?php echo $role->id ?>"><?php echo $role->name ?></option>
								 <?php } ?>
							</select>
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Password</label>
						</div>
						<div class="controls col-lg-4">
							<input type="password" name="password" class="form-control" value="" placeholder="new password">
						</div>
						<div class="controls col-lg-4">
							<input type="password" name="re_password" class="form-control" value="" placeholder="re-enter password">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-label col-lg-2">
						<label class="pull-left">Telepon<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-4">
						<input type="text" name="phone" class="form-control" value="<?php echo isset($value['phone']) ? $value['phone'] : null ?>">
					</div>
					<!-- <div class="control-group"> -->
						<!-- <div class="control-label col-lg-2">
							<label class="pull-left">Provinsi<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-3">
							<select name="province_id" class="form-control" id="province_id">
								<?php foreach ($provinces->result() as $province){ ?>
									<option value="<?php echo $province->id ?>" <?php echo isset($value['province_id']) ? ($province->id == $value['province_id'] ? 'selected' : '') : '' ?>><?php echo $province->province ?></option>
								 <?php } ?>
							</select>
						</div> -->
						<div class="control-label col-lg-1">&nbsp;</div>
						<!-- <div class="control-label col-lg-2">
							<label class="pull-left">Kota<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-3">
							<select name="city_id" class="form-control" id="city_id">
								 <?php foreach ($cities->result() as $city){ ?>
									<option value="<?php echo $city->id ?>"><?php echo $city->city_name ?></option>
								 <?php } ?> -->
							<!-- </select> -->
						<!-- </div> -->
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<label class="pull-left">Alamat</label>
						</div>
						<div class="controls col-lg-9">
							<textarea name="address" value="<?php echo isset($value['address']) ? $value['address'] : null ?>" class="form-control"><?php echo isset($value['address']) ? $value['address'] : null ?></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>users" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>