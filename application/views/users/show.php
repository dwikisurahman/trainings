<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Lengkap</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $user->name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Email</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $user->email ?>
					</div>
				</div>
				<div class="clearfix"><br></div>
			
				<div class="control-label col-lg-2">
					<label class="pull-left">Username</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $user->username ?>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>users" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>users/edit/<?php echo $user->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>