<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Module</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $module->name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Child</label>
					</div>
					<div class="controls col-lg-2">
						<?php if($module->child != null){ ?>
							<?php echo $this->db->query('SELECT name FROM modules WHERE id = '.$module->child)->row()->name; ?>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>

			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Path Module</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $module->path_module ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Module Class</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $module->module_class ?>
				</div>
			</div>
			<div class="clearfix"><br></div>

			<div class="control-group">
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Active Class</label>
					</div>
					<div class="controls col-lg-3">
						<?php echo $module->active_class ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Deskripsi</label>
					</div>
					<div class="controls col-lg-3">
						<?php echo $module->description ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>modules" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>modules/edit/<?php echo $module->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>