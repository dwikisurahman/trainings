<form class="form-horizontal" action="<?=base_url();?>modules/index" method="get">
	<div class="col-sm-3" style="padding-left:0px">
		<input type="text" name="name" placeholder="Nama..." class="form-control input-sm" id="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>">
	</div>	
	<div class="col-lg-3">
		<div class="form-group input-group" style="margin:0">
			<input type="text" name="path" placeholder="Path..." class="form-control input-sm" id="path" value="<?php echo isset($_GET['path']) ? $_GET['path'] : '' ?>">
			<span class="input-group-btn">
				<a href="<?=base_url();?>modules/index" class="btn btn-white reset btn-sm"><i class="fa fa-refresh"></i></a>
				<?php echo form_submit(array('class' => 'btn btn-primary btn-sm', 'value' => 'Cari')); ?>
			</span>
		</div>
	</div>
</form>
<a href="<?=base_url();?>modules/add" class="btn btn-default waves-effect waves-light pull-right btn-sm"><i class="fa fa-plus"></i> Tambah
</a>
<div class="clearfix"></div>
<br>