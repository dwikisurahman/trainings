<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('modules/create'); ?>
<?php }else{ ?>
	<?php echo form_open('modules/update'); ?>
<?php }?>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="col-sm-8">
					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Nama Module<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-8">
							<input type="text" name="name" class="form-control" value="<?php echo isset($value['name']) ? $value['name'] : null ?>">
							<input type="hidden" class="form-control" name="id" value="<?php echo isset($module) ? $module->id : isset($value['id']) ? $value['id'] : null; ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Child<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-4">
							<select name="child" class="form-control">
								<?php foreach ($childs->result() as $child){ ?>
									<option value="<?php echo $child->id ?>" <?php echo isset($value['child']) ? ($child->id == $value['child'] ? 'selected' : '') : '' ?>><?php echo $child->name ?></option>
								 <?php } ?>
							</select>
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>

					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Path Module<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="path_module" class="form-control" value="<?php echo isset($value['path_module']) ? $value['path_module'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>

					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Module Class<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="module_class" class="form-control" value="<?php echo isset($value['module_class']) ? $value['module_class'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>

					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Active Class<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="active_class" class="form-control" value="<?php echo isset($value['active_class']) ? $value['active_class'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>

					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Model Class<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="model_class" class="form-control" value="<?php echo isset($value['model_class']) ? $value['model_class'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>
					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Menu index<span class="required">*</span></label>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="menu_order" class="form-control" value="<?php echo isset($value['menu_order']) ? $value['menu_order'] : null ?>">
						</div>
					</div>
					<div class="clearfix"><br><br><br></div>

					<div class="control-group">
						<div class="control-label col-lg-4">
							<label class="pull-left">Deskripsi</label>
						</div>
						<div class="controls col-lg-8">
							<textarea name="description" value="<?php echo isset($value['description']) ? $value['description'] : null ?>" class="form-control"><?php echo isset($value['description']) ? $value['description'] : null ?></textarea>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>modules" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>