<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('receipts/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
				    	<th width="3%">No</th>
				    	<th width="20%">No. Kwitansi</th>
						<th width="15%">Nama Diklat</th>
						<th width="20%">No. Invoice</th>
						<th width="15%">Penerima</th>
						<th width="15%">Status</th>
						<th width="15%">Tgl buat</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($receipts) == 0 || $receipts->num_rows() == null){
	                            echo"<td colspan='8'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($receipts->result() as $example) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $example->receipt_no ?></td>
								<td><?php echo $example->training_name ?></td>
								<td><?php echo $example->invoice_no ?></td>
								<td><?php echo $example->receiver ?></td>
								<td><?php echo $example->status == 'pending' ? '<a class="btn btn-sm btn-warning">pending</a>' : '<a class="btn btn-info">'.$example->status.'</a>' ?></td>
								<td><?php echo date("d F Y", strtotime($example->created_at)) ?></td>
								<td>
									<a href="<?= base_url(); ?>receipts/show/<?php echo $example->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>receipts/edit/<?php echo $example->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>receipts/vprint/<?php echo $example->id ?>" class="btn btn-warning btn-sm" title="Detail"><i class="fa fa-print"></i></a>
									<a href="<?= base_url(); ?>receipts/destroy/<?php echo $example->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $receipts_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $receipts_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>