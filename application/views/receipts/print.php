<div class="row">
	<div class="col-sm-12">
		<div class="card-box">

		<body >

<table class='tbl_header'>
      <tr>
        <td rowspan='6' style='width:160px; text-align:center; '><img src="<?= base_url(); ?>assets/images/lp.png" width='100' height='111'/></td>
        <td style='width:1247px; text-align:center;   font-family:Arial, Helvetica, sans-serif; '>PEMERINTAH PROVINSI JAWA BARAT</td>
      </tr>
      <tr>
        <td style=' text-align:center;  font-family:Arial, Helvetica, sans-serif; '><font size='3' ><b>RUMAH SAKIT JIWA</b></font></td>
      </tr>
      <tr>
        <td align='center'>Jalan Kolonel Masturi KM. 7</td>
      </tr>
      <tr>
        <td align='center'>Telepon: (022) 2700260 Faksimil: (022) 2700304</td>
      </tr>
      <tr>
        <td align='center'>Website: www.rsj.jabarprov.go.id email: rsj@jabarprov.go.id</td>
      </tr>
      <tr>
        <td align='center'>KABUPATEN BANDUNG BARAT - 40551</td>
      </tr>
      <tr>
        <td colspan='2' align='center'><hr width='100%' size='2' color='#000000'></td>
      </tr>
    </table>

<h3 align="center" style="margin-top: 5px; font-size: 25px; font-weight: bold;">
  <b><U>KWITANSI</U></b>
</h3>

<table class="invoice-logo" cellspacing="0" style="margin-top: 5px; font-weight: 80;">
  <tr>
    <th width="50px;" align="border-right"></th>
    <th width="1px;" align="border-center"></th>
    <th width="200px" align="border-right"></th>
  </tr>
  <tr>
    <th align="left">No. Kuitansi</th>
    <th align="center">:</th>
    <td align="left"><?php echo $receipts->receipt_no?></td>
  </tr>
  <tr>
    <th align="left">Sudah diterima dari</th>
    <th align="center">:</th>
    <td align="left"><?php echo $receipts->receiver?></td>
  </tr>
  <tr>
    <th align="left">Uang Sebesar</th>
    <th align="center">:</th>
    <?php get_instance()->load->helper('site'); ?>
    <td align="left"><?php echo terbilang($receipts->total_amount) ?></td>
  </tr>
  <tr>
    <th align="left">Uang Pembayaran</th>
    <th align="center">:</th>
    <td align="left"><?php echo $receipts->for_payment?>
  <tr>
    <th align="left">Rekomendasi</th>
    <th align="center">:</th>
    <td align="left"> <?php echo $receipts->recomendation_no?></td>
  </tr>
</table><br>
<table>
  <tr >
  	<th width="10px;" align="border-right"></th>
  	<th width="100px;" align="border-right"></th>
    <th width="100px;" align="border-right"></th>
    <th width="200px;" align="border-right"></th>
    <th width="300px" align="border-right"></th>
    <th width="50px" align="border-center"></th>
  </tr>
   <tr>
   	<th></th>
  	<th style=" border: 2px solid #000">Jumlah Rp. <?php echo number_format($receipts->total_amount) ?>,-</th>
    <th></th>
    <th></th>
    <th></th>
    <th align="center">Bandung Barat, <?php echo date("d F Y", strtotime($receipts->created_at)) ?> </th>
    <th></th>
  </tr>
  <tr>
  	<th></th>
  	<th></th>
  	<th></th>
  	<td></td>
  	<th ></th>
    <th align="center">Bendahara Penerimaan Pembantu</th>
  </tr>
</table><br><br>
<table>
 	<tr >
    <th width="100px;" align="border-right"></th>
    <th width="100px;" align="border-right"></th>
    <th width="100px;" align="border-right"></th>
    <th width="200px;" align="border-right"></th>
    <th width="300px" align="border-right"></th>
    <th width="120px" align="border-center"></th>
  </tr>
	<tr>
      <th></th>
  		<th></th>
  		<th></th>
  		<td></td>
  		<td></td>
  		<th align="center"> <?php echo $receipts->responsibility ?> </th>
  	</tr>
  	<tr>
      <th></th>
  		<th></th>
  		<th></th>
  		<td></td>
  		<td></td>
  		<th align="center">NIP. <?php echo $receipts->nip ?></th>
  	</tr>
</table>
</body>

		<script>
		    window.print();
		  </script>
		</div>
	</div>
</div>