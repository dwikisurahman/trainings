<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('receipts/create'); ?>
<?php }else{ ?>
	<?php echo form_open('receipts/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="control-group">
					<div class="control-group">
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">Nama Diklat<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<select class="form-control" name="training_id" id="training_id">
									<option value="">Pilih Nama DIklat</option>
									<?php foreach ($trainings as $training) { ?>
									<option value="<?php echo $training['id']?>" <?php echo isset($value['training_id']) ? $value['training_id'] == $training['id'] ? 'selected' : '' : null ?>><?php echo $training['name'] ?> </option>
									<?php } ?>
								</select>			
							</div>
							<div class="control-label col-lg-2">
								<label class="pull-left">No Invoice<span class="required">*</span></label>
							</div>				
							<div class="controls col-lg-3">
								<select class="form-control" name="invoice_id" id="invoice_id">
									<option value="">Pilih No Invoice</option>
									<?php foreach ($invoices as $invoice) { ?>
									<option value="<?php echo $invoice['id']?>" <?php echo isset($value['invoice_id']) ? $value['invoice_id'] == $invoice['id'] ? 'selected' : '' : null ?>><?php echo $invoice['invoice_no'] ?> </option>
									<?php } ?>
								</select>		
							</div>
						</div>
					<div class="clearfix"></div><br>
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">No kwitansi<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<input type="text" name="receipt_no" class="form-control" value="<?php echo isset($value['receipt_no']) ? $value['receipt_no'] : null ?>">			
							</div>
							<div class="control-label col-lg-2">
								<label class="pull-left">Pemerima<span class="required">*</span></label>
							</div>				
							<div class="controls col-lg-3">
								<input type="text" name="receiver" class="form-control" value="<?php echo isset($value['receiver']) ? $value['receiver'] : null ?>">		
							</div>
						</div>
						<div class="clearfix"><br><br><br></div>
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">Penanggung jawab<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<input type="text" name="responsibility" class="form-control" value="<?php echo isset($value['responsibility']) ? $value['responsibility'] : null ?>">						
							</div>
							<div class="control-label col-lg-2">
								<label class="pull-left">NIP<span class="required">*</span></label>
							</div>				
							<div class="controls col-lg-3">
								<input type="text" name="nip" class="form-control" value="<?php echo isset($value['nip']) ? $value['nip'] : null ?>">		
							</div>
						</div>

						<div class="clearfix"><br><br><br></div>
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">Rekomendasi<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<input type="text" name="recomendation_no" class="form-control" value="<?php echo isset($value['recomendation_no']) ? $value['recomendation_no'] : null ?>">						
							</div>
						</div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<label class="pull-left">Untuk Pembayaran<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<input type="text" name="for_payment" class="form-control" value="<?php echo isset($value['for_payment']) ? $value['for_payment'] : null ?>">						
							</div>
						</div>
						<div class="clearfix"><br><br><br></div>
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">Deskripsi</label>
							</div>
							<div class="controls col-lg-3">
								<textarea name="description" value="<?php echo isset($value['description']) ? $value['description'] : null ?>" class="form-control"><?php echo isset($value['description']) ? $value['description'] : null ?></textarea>
							</div>
						</div>
						<!-- <?php 
						if ($user->role_id == 1){?>
						<div class="control-group">
							<div class="control-label col-lg-3">
								<label class="pull-left">Status<span class="required">*</span></label>
							</div>
							<div class="controls col-lg-3">
								<select name='status' class="form-control">
		                            <option value=''>- Pilih Status -</option>
		                            <option value='pending' <?php echo isset($value['status']) ? $value['status'] == 'pending' ? 'selected' : "" : "" ?>>Pending</option>
		                            <option value='submit' <?php echo isset($value['status']) ? $value['status'] == 'submit' ? 'selected' : "" : "" ?>>Ajukan</option>
		                            <option value='approve' <?php echo isset($value['status']) ? $value['status'] == 'approve' ? 'selected' : "" : "" ?>>Diterima</option>
		                        </select>
							</div>
						</div>
					<?php } ?>	 -->			
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<a href="<?= base_url(); ?>receipts" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
					<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>