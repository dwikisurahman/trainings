<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Id Diklat</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $receipt->training_id ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Id Invoice</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->invoice_id ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">NO Kwitansi</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $receipt->receipt_no ?>
				</div>
				<div class="control-label col-lg-2">
					<label class="pull-left">Penerima</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $receipt->receiver ?>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Penanggung jawab</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->responsibility ?>
					</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">NIP</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->nip ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Rekomendasi</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->recomendation_no ?>
					</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Status</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->status ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Untuk Pembayaran</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->for_payment ?>
					</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">Description</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $receipt->description ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>receipts" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>receipts/edit/<?php echo $receipt->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>