<div class="row">
	<div class="col-md-3 col-lg-3">
		<div class="widget-bg-color-icon card-box fadeInDown animated">
			<div class="bg-icon bg-icon-info pull-left">
				<i class="md md-attach-money text-info"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="counter">31,570</b></h3>
				<p class="text-muted">Total Revenue</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-6 col-lg-3">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-pink pull-left">
				<i class="md md-add-shopping-cart text-pink"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="counter">280</b></h3>
				<p class="text-muted">Today's Sales</p>
			</div>
				<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-6 col-lg-3">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-purple pull-left">
				<i class="md md-equalizer text-purple"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="counter">0.16</b>%</h3>
				<p class="text-muted">Conversion</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-6 col-lg-3">
		<div class="widget-bg-color-icon card-box">
			<div class="bg-icon bg-icon-success pull-left">
				<i class="fa fa-users text-success"></i>
			</div>
			<div class="text-right">
				<h3 class="text-dark"><b class="counter">64.000</b></h3>
				<p class="text-muted">Total pengguna</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script type="text/javascript" src="plugins/jQuery/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#container').highcharts({
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: ''
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>Peserta ' + this.series.name + '</b><br/>' +
                        'Berjumlah ' + this.point.y + ' Orang';
                }
            }
        });
    });
</script>
<div class="box box-success">
    <div class="box-header">
    <i class="fa fa-th-list"></i>
    <h3 class="box-title">Data Mahasiswa Praktek Tahun 2020</h3>
        <div class="box-tools pull-right">
           <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
        </div>
        </div>

<div class="box-body chat" id="chat-box">
    <script src="plugins/highchart/highcharts.js"></script>
    <script src="plugins/highchart/modules/data.js"></script>
    <script src="plugins/highchart/modules/exporting.js"></script>
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable" style='display:none'>
    <thead>
        <tr>
            <th></th>
            <th>Keperawatan</th>
            <th>Kedokteran</th>
            <th>Rekam Medis</th>
			<th>Farmasi</th>
        </tr>
    </thead>
    <tbody>
   
    </tbody>
</table>
</div><!-- /.chat -->
</div><!-- /.box (chat box) -->
		