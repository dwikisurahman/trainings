<style type="text/css">
	.notice-notice{
		background-color: #7EE09E;
		padding: 15px;
		color: #0e7139;
		border-radius: 5px;
	}
</style>

<?php if ($this->session->flashdata('alert') != ""){ ?>
	<div class="alert alert-dismissable alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<b><?php echo $this->session->flashdata('alert') ?></b>
	</div>
<?php } ?>

<?php if ($this->session->flashdata('success') != ""){ ?>
	<div class="notice notice-dismissable notice-notice">
		<button type="button" class="close" data-dismiss="notice">×</button>
		<b><?php echo $this->session->flashdata('success') ?></b>
	</div>	
	<br>
<?php } ?>

<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
	$(".close").on("click", function(){
		$(this).parent().remove();
	})
</script>