<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php if (!empty($title)) echo $title . ' - '; ?>Diklat RSJ Cisarua</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo-acd.png" />
        <!-- plugins:css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/ionicons/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/typicons/src/font/typicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/css/vendor.bundle.addons.css">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/shared/style.css">
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo_1/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/paper.css">
        <!-- End Layout styles -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.0.min.js"></script>
        <style>
            page {
                background: white;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
            }
        
            .split {
                margin: 20px 0px;
                border-top: 1px dashed #ccc;
            }
            body{
                font-family: 'Arial';
                font-size: 14px;
                font-weight: bold;
                margin: 0;
                padding: 0;
                line-height: 1.2;
            }
            .invoice-logo{
                height: 35px;
            }
            .invoice-logo img{
                height: 110px;
            }
            table{
                width: 100%;
                border-spacing: 0;
            }
            table.bordered{
                width: 20%;
                border-top: 1px solid #000;
                border-left: 1px solid #000;
                border-spacing: 0;
            }
            table th,
            table td{
                padding: 2px;/*
                border-bottom: 1px solid #000;
                border-right: 1px solid #000;*/
            }
            table.bordered th,
            table.bordered td{
                border-bottom: 1px solid #000;
                border-right: 1px solid #000;
            }
            .invoice-to{
                border: 1px solid #000;
                padding: 1.6px;
                line-height: 1.3;
            }
            .invoice-payment{
                line-height: 1.3;
            }
        </style>
    </head>

    <?php echo $content; ?>
    <script>
        window.print();
    </script>
</html>