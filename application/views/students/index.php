<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('students/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
				    	<th width="3%">No</th>
				    	<th width="10%">Diklat</th>
				    	<th width="10%">NIM</th>
						<th width="15%">Nama Mahasiswa</th>
						<th width="15%">Email</th>
						<th width="15%">Instansi</th>
						<th width="15%">Jurusan</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($students) == 0 || $students->num_rows() == null){
	                            echo"<td colspan='8'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($students->result() as $student){ ?>

							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $student->training_name ?></td>
								<td><?php echo $student->nim ?></td>
								<td><?php echo $student->name ?></td>
								<td><?php echo $student->email ?></td>
								<td><?php echo $student->instance ?></td>
								<td><?php echo $student->majors ?></td>
								<td>
									<a href="<?= base_url(); ?>students/show/<?php echo $student->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>students/edit/<?php echo $student->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>students/destroy/<?php echo $student->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $students_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $students_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>