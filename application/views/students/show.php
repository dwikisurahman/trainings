<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Nama Mahasiswa</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $student->name ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">NIM</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $student->nim ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">email</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $student->email ?>
				</div>
				<div class="control-group">
					<div class="control-label col-lg-2">
						<label class="">ID Training</label>
					</div>
					<div class="controls col-lg-2">
						<?php echo $student->training_id ?>
					</div>
				</div>
			</div>
			<div class="clearfix"><br></div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="">Instansi</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $student->instance ?>
				</div>
			</div>
			<div class="control-group">
				<div class="control-label col-lg-2">
					<label class="pull-left">Jurusan</label>
				</div>
				<div class="controls col-lg-3">
					<?php echo $student->majors ?>
				</div>
			</div>
			<div class="clearfix"><br></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<a href="<?= base_url(); ?>students" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a href="<?= base_url(); ?>students/edit/<?php echo $student->id ?>" class="btn btn-success pull-right btn-sm"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	</div>
</div>