<?php $this->load->view('shared/errors'); ?>
<?php if ($this->uri->segment(2) == 'create' || $this->uri->segment(2) == 'add') { ?>
	<?php echo form_open('students/create'); ?>
<?php }else{ ?>
	<?php echo form_open('students/update'); ?>
<?php }?>
	<input type="hidden" name="base_url" value="<?= base_url() ?>" id="base_url">
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Diklat<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<select class="form-control" name="training_id" id="training_id">
									<option value="">Pilih Nama DIklat</option>
									<?php foreach ($trainings as $training) { ?>
									<option value="<?php echo $training['id']?>" <?php echo isset($value['training_id']) ? $value['training_id'] == $training['id'] ? 'selected' : '' : null ?>><?php echo $training['name'] ?> </option>
									<?php } ?>
								</select>
						<input type="hidden" class="form-control" name="id" value="<?php echo isset($student) ? $student->id : isset($value['id']) ? $value['id'] : null; ?>">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Nama<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input type="text" name="name" class="form-control" value="<?php echo isset($value['name']) ? $value['name'] : null ?>">
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">NIM<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input name="nim" value="<?php echo isset($value['nim']) ? $value['nim'] : null ?>" class="form-control"></input>
					</div>
				</div>
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Email<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input name="email" value="<?php echo isset($value['email']) ? $value['email'] : null ?>" class="form-control"></input>
					</div>
				</div>		
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Instansi<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input name="instance" value="<?php echo isset($value['instance']) ? $value['instance'] : null ?>" class="form-control"></input>
					</div>
				</div>	
				<div class="clearfix"><br><br><br></div>
				<div class="control-group">
					<div class="control-label col-lg-3">
						<label class="pull-left">Jurusan<span class="required">*</span></label>
					</div>
					<div class="controls col-lg-8">
						<input name="majors" value="<?php echo isset($value['majors']) ? $value['majors'] : null ?>" class="form-control"></input>
					</div>
				</div>										
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<a href="<?= base_url(); ?>students" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
				<button class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>