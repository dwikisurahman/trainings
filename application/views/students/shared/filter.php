<form class="form-horizontal" action="<?=base_url();?>students/index" method="get">
	<div class="col-sm-3" style="padding-left:0px">
		<input type="text" name="name" placeholder="Nama..." class="form-control input-sm" id="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>">
	</div>
	<div class="col-sm-2">
		<input type="text" name="no_medtrack" placeholder="Nomor Medtrack..." class="form-control input-sm" id="no_medtrack" value="<?php echo isset($_GET['no_medtrack']) ? $_GET['no_medtrack'] : '' ?>">
	</div>
	<div class="col-sm-2">
		<input type="text" name="date_join_start" placeholder="Daftar Dari..." class="form-control input-sm datepicker" id="date_join_start" value="<?php echo isset($_GET['date_join_start']) ? $_GET['date_join_start'] : '' ?>">
	</div>
	<div class="col-lg-3">
		<div class="form-group input-group" style="margin:0">
			<input type="text" name="date_join_end" placeholder="Daftar Sampai..." class="form-control input-sm datepicker" id="date_join_end" value="<?php echo isset($_GET['date_join_end']) ? $_GET['date_join_end'] : '' ?>">
			<span class="input-group-btn">
				<a href="<?=base_url();?>students/index" class="btn btn-white reset btn-sm"><i class="fa fa-refresh"></i></a>
				<?php echo form_submit(array('class' => 'btn btn-primary btn-sm', 'value' => 'Cari')); ?>
			</span>
		</div>
	</div>
</form>
<a href="javascript:void(0);" class="btn btn-success" onclick="formToggle('importFrm');"><i class="plus"></i> Import</a><div class="clearfix"></div><br>
	<a href="<?php echo base_url("csv/format.csv"); ?>">Download Format CSV</a>
	<div class="col-md-12" id="importFrm" style="display: none;">
            <form action="<?php echo base_url('students/import'); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="file" />
                <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
            </form>
        </div>
    <script>
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}
</script>