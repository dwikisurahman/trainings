 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Invoice_model');
		$this->load->model('User_model');
		$this->load->helper('form');
		$this->load->library('pagination'); 
		$this->load->helper('date');
		$this->load->helper('url');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Data Invoice',
			'description' => 'Informasi Data Invoice',
			'invoices_all' => $this->Invoice_model->get_invoices($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'invoices/index/',
			'total_rows' => $data['invoices_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['invoices'] = $this->Invoice_model->get_invoices($filter, $config['per_page'], $from, null);
		$this->middle = 'invoices/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Data Invoice',
			'description' => 'Tambah Data Invoice'
		);
		$this->middle = 'invoices/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Invoice';
		$data['description'] = 'Informasi Data Invoice';
		$data['open_data_sales'] = 'active';
		$data['active_data_master_invoice'] = 'active';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('file_upload', 'file_upload', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$this->middle = 'invoices/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'invoice_no' => $this->input->post('invoice_no', true),
				'file_upload' => $this->input->post('file_upload', true),				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Invoice_model->create_invoice($data);
			redirect('invoices/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Invoice_model->id = $id;
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Detail Invoice',
			'description' => 'Informasi Detail Invoice',
			'invoice' => $this->Invoice_model->get_invoices()->row()
		);

		$this->middle = 'invoices/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Edit Invoice',
			'description' => 'Form Edit Invoice',
			'users' => $this->User_model->get_users()->row()
		);

		$this->Invoice_model->id = $this->uri->segment(3);
		$data['invoice'] = $this->Invoice_model->get_invoices()->row();
		$invoice = $data['invoice'];
		$data['value'] = array(
			'id' => $invoice->id,
			'invoice_no' => $invoice->invoice_no,
			'file_upload' => $invoice->file_upload,
			'status' => $invoice->status
		);
		$this->middle = 'invoices/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data invoices';
		$data['description'] = 'Informasi Data invoices';
		$data['open_data_master'] = 'active';
		$data['active_data_master_invoice'] = 'active';
		

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('invoice_no', 'invoice_no', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'invoice_no' => $this->input->post('invoice_no', true),
				'status' => $this->input->post('status', true),
			);
			$this->middle = 'invoices/new';
			$this->data = $data;
			$this->layout();
		}else{
			
			$file_upload =  $_FILES['file_upload'];
			$config['upload_path'] = 'cdn/images/invoices'; //path folder
        	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
   /*     $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload*/
 
	        $this->load->library('upload', $config);
	        if(!empty($_FILES['file_upload']['name'])){
	 			
	            if ($this->upload->do_upload('file_upload')){
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='cdn/images/invoices'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '50%';
	                $config['width']= 600;
	                $config['height']= 400;
	                $config['new_image']= 'cdn/images/invoices'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

	               $file_upload= $this->upload->data('file_name');
	                echo "Image berhasil diupload";
	            }
	        }else{
	            echo "Image yang diupload kosong";
	        }
				$data = array(
					'invoice_no' => $this->input->post('name', true),
					'status' => $this->input->post('status', true),
					'created_at' => mdate('%Y-%m-%d'),
					'file_upload' => $file_upload
				);
				$this->Invoice_model->id = $id;
				$this->Invoice_model->update_invoice($data);
				$this->session->set_flashdata('success','Data berhasil di perbaharui');
				redirect('invoices/show/'.$id);

		}
	}

	public function vprint(){
		$id = $this->uri->segment(3);
		$this->Invoice_model->id = $id;
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Detail Invoice',
			'description' => 'Informasi Detail Invoice',
			'invoice' => $this->Invoice_model->get_invoices()->row()
		);
		$this->load->view('invoices/print', $data);
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Invoice_model->id = $this->uri->segment(3);
		$this->Invoice_model->destroy();
		redirect('invoices/index');
	}
}