<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipts extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Receipt_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
		$this->load->library('upload');
    	} 

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Data Receipt',
			'description' => 'Informasi Data Receipt',
			'receipts_all' => $this->Receipt_model->get_receipts($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'receipts/index/',
			'total_rows' => $data['receipts_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['receipts'] = $this->Receipt_model->get_receipts($filter, $config['per_page'], $from, null); 
		$this->middle = 'Receipts/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$filter = "";
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Data Receipt',
			'description' => 'Tambah Data Receipt',
			
		);
		$data['trainings'] = $this->Receipt_model->get_data_selects('trainings');
		$data['invoices'] = $this->Receipt_model->get_data_selects('invoices');
		$this->middle = 'Receipts/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Receipt';
		$data['description'] = 'Informasi Data Receipt';
		$data['open_data_sales'] = 'active';
		$data['active_data_master_Receipt'] = 'active';
		$data['trainings'] = $this->Receipt_model->get_data_selects('trainings');
		$data['invoices'] = $this->Receipt_model->get_data_selects('invoices');

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('training_id', 'training_id', 'required');	
		$this->form_validation->set_rules('invoice_id', 'invoice_id', 'required');
		$this->form_validation->set_rules('receiver', 'receiver', 'required');	
		$this->form_validation->set_rules('responsibility', 'responsibility', 'required');	
		$this->form_validation->set_rules('nip', 'nip', 'required');	
		$this->form_validation->set_rules('receipt_no', 'receipt_no', 'required');	
		$this->form_validation->set_rules('recomendation_no', 'recomendation_no', 'required');
		$this->form_validation->set_rules('for_payment', 'for_payment', 'required');

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'training_id' => $this->input->post('training_id', true),
				'invoice_id' => $this->input->post('invoice_id', true),
				'receipt_no' => $this->input->post('receipt_no', true),
				'receiver' => $this->input->post('receiver', true),
				'for_payment' => $this->input->post('for_payment', true),
				'responsibility' => $this->input->post('responsibility', true),
				'nip' => $this->input->post('nip', true),
				'recomendation_no' => $this->input->post('recomendation_no', true),
				'status' => $this->input->post('status', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'Receipts/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'training_id' => $this->input->post('training_id', true),
				'invoice_id' => $this->input->post('invoice_id', true),
				'receipt_no' => $this->input->post('receipt_no', true),
				'receiver' => $this->input->post('receiver', true),
				'for_payment' => $this->input->post('for_payment', true),
				'responsibility' => $this->input->post('responsibility', true),
				'nip' => $this->input->post('nip', true),
				'recomendation_no' => $this->input->post('recomendation_no', true),
				'status' => $this->input->post('status', true),
				'description' => $this->input->post('description', true)				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Receipt_model->create_Receipt($data);
			redirect('Receipts/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Receipt_model->id = $id;
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Detail Receipt',
			'description' => 'Informasi Detail Receipt',
			'receipt' => $this->Receipt_model->get_receipts()->row(),
		);
		$data['trainings'] = $this->Receipt_model->get_data_selects('trainings');
		$data['invoices'] = $this->Receipt_model->get_data_selects('invoices');

		$this->middle = 'Receipts/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Edit Receipt',
			'description' => 'Form Edit Receipt',
		);

		$this->Receipt_model->id = $this->uri->segment(3);
		$data['receipt'] = $this->Receipt_model->get_receipts()->row();
		$data['trainings'] = $this->Receipt_model->get_data_selects('trainings');
		$data['invoices'] = $this->Receipt_model->get_data_selects('invoices');
		$receipt = $data['receipt'];
		$data['value'] = array(
			'id' => $receipt->id,
			'invoice_id' => $receipt->invoice_id,
			'training_id'=> $receipt->training_id,
			'receiver' => $receipt->receiver,
			'for_payment' => $receipt->for_payment,
			'receipt_no' => $receipt->receipt_no,
			'responsibility'=> $receipt->responsibility,
			'nip' => $receipt->nip,
			'recomendation_no' => $receipt->recomendation_no,
			'status' => $receipt->status,
			'description' => $receipt->description
		);
		$this->middle = 'Receipts/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Receipt';
		$data['description'] = 'Informasi Data Receipt';
		$data['open_data_sales'] = 'active';
		$data['active_data_Receipt'] = 'active';
		$data['trainings'] = $this->Receipt_model->get_data_selects('trainings');
		$data['invoices'] = $this->Receipt_model->get_data_selects('invoices');

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('training_id', 'training_id', 'required');	
		$this->form_validation->set_rules('invoice_id', 'invoice_id', 'required');
		$this->form_validation->set_rules('receiver', 'receiver', 'required');	
		$this->form_validation->set_rules('responsibility', 'responsibility', 'required');	
		$this->form_validation->set_rules('nip', 'nip', 'required');	
		$this->form_validation->set_rules('receipt_no', 'receipt_no', 'required');	
		$this->form_validation->set_rules('recomendation_no', 'recomendation_no', 'required');
		$this->form_validation->set_rules('for_payment', 'for_payment', 'required');		

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'training_id' => $this->input->post('training_id', true),
				'invoice_id' => $this->input->post('invoice_id', true),
				'receipt_no' => $this->input->post('receipt_no', true),
				'receiver' => $this->input->post('receiver', true),
				'for_payment' => $this->input->post('for_payment', true),
				'responsibility' => $this->input->post('responsibility', true),
				'nip' => $this->input->post('nip', true),
				'recomendation_no' => $this->input->post('recomendation_no', true),
				'status' => $this->input->post('status', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'Receipts/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'training_id' => $this->input->post('training_id['.$example->id.']', true),
				'invoice_id' => $this->input->post('invoice_id['.$example->id.']', true),
				'receipt_no' => $this->input->post('receipt_no', true),
				'receiver' => $this->input->post('receiver', true),
				'for_payment' => $this->input->post('for_payment', true),
				'responsibility' => $this->input->post('responsibility', true),
				'nip' => $this->input->post('nip', true),
				'recomendation_no' => $this->input->post('recomendation_no', true),
				'status' => $this->input->post('status', true),
				'description' => $this->input->post('description', true)
			);
			$this->Receipt_model->id = $id;
			$this->Receipt_model->update_Receipt($data);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('Receipts/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Receipt_model->id = $this->uri->segment(3);
		$this->Receipt_model->destroy();
		redirect('Receipts/index');
	}

	public function vprint(){
		$data['receipts'] = $this->Receipt_model->get_receipts('receipts')->result();
		$data = array(
			'open_data_sales' => 'active',			
			'page_title' => 'Detail Invoice',
			'description' => 'Informasi Detail Invoice',
			'receipts' => $this->Receipt_model->get_receipts()->row(),
		);
		$this->load->view('receipts/print', $data);
	}

	function change(){
		//$this->simple_login->check_role();
		$data = array('status' => $this->input->get('approve', true));
		$this->Receipt_model->id = $this->uri->segment(3);
		$this->Receipt_model->update_receipt($data);
		redirect('trainings/index');	
	}
}