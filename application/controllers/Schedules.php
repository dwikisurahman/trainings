<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Schedule_model');
		$this->load->model('Mdroom_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_administration' => 'active',			
			'page_title' => 'Data Schedule',
			'description' => 'Informasi Data Schedule',
			'schedules_all' => $this->Schedule_model->get_schedules($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'schedules/index/',
			'total_rows' => $data['schedules_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['schedules'] = $this->Schedule_model->get_schedules($filter, $config['per_page'], $from, null);
		$this->middle = 'schedules/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$filter = array();
		$data = array(
			'open_data_administration' => 'active',			
			'page_title' => 'Data Schedule',
			'description' => 'Tambah Data Schedule',
			'mdrooms' => $this->Mdroom_model->get_mdrooms($filter, null, null,'true'),
		);
		$this->middle = 'schedules/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Schedule';
		$data['description'] = 'Informasi Data Schedule';
		$data['open_data_administration'] = 'active';
		$data['active_data_master_schedule'] = 'active';
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('mentor_name', 'mentor_name', 'required');
		$this->form_validation->set_rules('program_study', 'program_study', 'required');
		$this->form_validation->set_rules('mdroom_id', 'mdroom_id', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'mentor_name' => $this->input->post('mentor_name', true),
				'program_study' => $this->input->post('program_study', true),
				'mdrooms_id' => $this->input->post('mdrooms_id', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'schedules/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'mentor_name' => $this->input->post('mentor_name', true),
				'program_study' => $this->input->post('program_study', true),
				'mdroom_id' => $this->input->post('mdroom_id', true),
				'description' => $this->input->post('description', true)				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Schedule_model->create_schedule($data);
			redirect('schedules/show/'.$id);
		}
	}

	function show(){
		$filter = array();
		$id = $this->uri->segment(3);
		$this->Schedule_model->id = $id;
		$data = array(
			'open_data_administration' => 'active',			
			'page_title' => 'Detail Schedule',
			'description' => 'Informasi Detail Schedule',
			'schedule' => $this->Schedule_model->get_schedules()->row(),
			'mdrooms' => $this->Mdroom_model->get_mdrooms($filter, null, null,'true')
		);

		$this->middle = 'schedules/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_administration' => 'active',			
			'page_title' => 'Edit Schedule',
			'description' => 'Form Edit Schedule',
			'mdrooms' => $this->Mdroom_model->get_mdrooms($filter, null, null,'true')
		);

		$this->Schedule_model->id = $this->uri->segment(3);
		$data['schedule'] = $this->Schedule_model->get_schedules()->row();
		$schedule = $data['schedule'];
		$data['value'] = array(
			'id' => $schedule->id,
			'mentor_name' => $schedule->mentor_name,
			'program_study' => $schedule->program_study,
			'mdrooms_id' => $schedule->mdrooms_id,
			'description' => $schedule->description
		);
		$this->middle = 'schedules/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Schedule';
		$data['description'] = 'Informasi Data Schedule';
		$data['open_data_administration'] = 'active';
		$data['active_data_schedule'] = 'active';

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('mentor_name', 'mentor_name', 'required');
		$this->form_validation->set_rules('program_study', 'program_study', 'required');
		$this->form_validation->set_rules('mdroom_id', 'mdroom_id', 'required');		

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'mentor_name' => $this->input->post('mentor_name', true),
				'program_study' => $this->input->post('program_study', true),
				'mdroom_id' => $this->input->post('mdroom_id', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'schedules/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'mentor_name' => $this->input->post('mentor_name', true),
				'program_study' => $this->input->post('program_study', true),
				'mdrooms_id' => $this->input->post('mdrooms_id', true),
				'description' => $this->input->post('description', true)
			);
			$this->Schedule_model->id = $id;
			$this->Schedule_model->update_schedule($data);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('schedules/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Schedule_model->id = $this->uri->segment(3);
		$this->Schedule_model->destroy();
		redirect('schedules/index');
	}
}