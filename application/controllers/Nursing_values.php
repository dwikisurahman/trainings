<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nursing_values extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Nursing_value_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data Nursing_value',
			'description' => 'Informasi Data Nursing_value',
			'Nursing_values_all' => $this->Nursing_value_model->get_Nursing_values($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'Nursing_values/index/',
			'total_rows' => $data['Nursing_values_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['nursing_values'] = $this->Nursing_value_model->get_Nursing_values($filter, $config['per_page'], $from, null);
		$this->middle = 'nursing_values/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data Data Nilai Keperawatan',
			'description' => 'Tambah Data Data Nilai Keperawatan'
		);
		$this->middle = 'Nursing_values/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Data Nilai Keperawatan';
		$data['description'] = 'Informasi Data Data Nilai Keperawatan';
		$data['open_data_master'] = 'active';
		$data['active_data_master_Nursing_value'] = 'active';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'Nursing_values/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true),				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Nursing_value_model->create_Nursing_value($data);
			redirect('Nursing_values/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Nursing_value_model->id = $id;
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Detail Data Nilai Keperawatan',
			'description' => 'Informasi Detail Data Nilai Keperawatan',
			'Nursing_value' => $this->Nursing_value_model->get_Nursing_values()->row()
		);

		$this->middle = 'Nursing_values/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Edit Data Nilai Keperawatan',
			'description' => 'Form Edit Data Nilai Keperawatan'
		);

		$this->Nursing_value_model->id = $this->uri->segment(3);
		$data['Nursing_value'] = $this->Nursing_value_model->get_Nursing_values()->row();
		$Nursing_value = $data['Nursing_value'];
		$data['value'] = array(
			'id' => $Nursing_value->id,
			'name' => $Nursing_value->name,
			'description' => $Nursing_value->description
		);
		$this->middle = 'Nursing_values/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Data Nilai Keperawatan';
		$data['description'] = 'Informasi Data Data Nilai Keperawatan';
		$data['open_data_master'] = 'active';
		$data['active_data_Nursing_value'] = 'active';

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');		

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'Nursing_values/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->Nursing_value_model->id = $id;
			$this->Nursing_value_model->update_Nursing_value($data);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('nursing_values/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Nursing_value_model->id = $this->uri->segment(3);
		$this->Nursing_value_model->destroy();
		redirect('nursing_values/index');
	}

	public function nursing_values_excel()
	{
		// Load plugin PHPExcel nya
		include_once APPPATH ."libraries/PHPExcel.php";
			
		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('Diklat RSJ Cisarua')
						->setLastModifiedBy('Diklat RSJ Cisarua')
						->setTitle("Data Nilai Keperawatan")
						->setSubject("Nilai Keperawatan")
						->setDescription("Data Nilai Keperawatan")
						->setKeywords("Data Nilai Keperawatan");


		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		$excel->setActiveSheetIndex(0)->setCellValue('c1', "Data Nilai Keperawatan"); // Set kolom A1 
		$excel->getActiveSheet()->mergeCells('A1:M1'); // Set Merge Cell pada kolom A1 sampai M1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NIM"); // Set kolom 
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Nama Mahasiswa"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Instansi"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Jurusan"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "IP"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "SPTK"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Prepost"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Dokep"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "Tak"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "Penyuluhan"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "Prestasi"); // Set kolom
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "Sikap"); // Set kolom

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);

		// Panggil data
			if(isset($nursing_values) == 0 || $nursing_values->num_rows() == null){
	            echo"<td colspan='14'> <center><span> Data Tidak Tersedia </span></center> </td>";
	        }else{
	            $no = 1 + $this->uri->segment(3); foreach ($nursing_values->result() as $example) { 
		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $example['nim']);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $example['name']);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $example['instance']);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $example['majors']);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $example['ip']);
			$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $example['sptk']);
			$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $example['prepost']);
			$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $example['dokep']);
			$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $example['tak']);
			$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $example['counseling']);
			$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $example['achievement']);
			$excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $example['attitude']);
			
			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(30); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(30); // Set width kolom F
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(30); // Set width kolom G
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(30); // Set width kolom H
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(30); // Set width kolom I
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(30); // Set width kolom J
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(30); // Set width kolom K
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(30); // Set width kolom L
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(30); // Set width kolom M
		}
	}
		//Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Data Nilai Keperawatan");
		$excel->setActiveSheetIndex(0);

		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Data Nilai Keperawatan.xls"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		ob_end_clean();
 
		$write->save('php://output');
		
	}
}