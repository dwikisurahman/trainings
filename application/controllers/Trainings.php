<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainings extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Training_model');
		$this->load->model('Invoice_model');
		$this->load->model('User_model');
		$this->load->model('Mdfacility_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Data Diklat',
			'description' => 'Informasi Data Diklat',
			'trainings_all' => $this->Training_model->get_trainings($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'trainings/index/',
			'total_rows' => $data['trainings_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['trainings'] = $this->Training_model->get_trainings($filter, $config['per_page'], $from, null);
		$this->middle = 'trainings/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Data Diklat',
			'description' => 'Tambah Data Diklat',
			'users' => $this->User_model->get_users()->row()
		);
		$this->middle = 'trainings/new';
		$this->data = $data;
		$this->layout();
	}

	function add_facility(){
		$filter = "";
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Data Diklat Fasilitas',
			'description' => 'Tambah Data Diklat Fasilitas',
			'mdfacilities' => $this->Mdfacility_model->get_mdfacilities($filter, null, null,'true')
		);
		$this->middle = 'trainings/facilities/new';
		$this->data = $data;
		$this->layout();
	}

	function create_facility(){
		$filter = "";
		$data = array();
		$data['page_title'] = 'Data Diklat Fasilitas';
		$data['description'] = 'Informasi Data Diklat Fasilitas';
		$data['open_data_system'] = 'active';
		$data['active_data_master_training'] = 'active';		
		$mdfacilities = $this->Mdfacility_model->get_mdfacilities($filter, null, null,'true');
		foreach ($mdfacilities->result() as $example) {		
			$data = array(
				'training_id' => $this->input->post('training_id', true),
				'facility_id' => $this->input->post('facility_id['.$example->id.']', true),
				'frequency' => $this->input->post('frequency['.$example->id.']', true),
				'created_at' => mdate('%Y-%m-%d'),
				'price' => $example->price * $this->input->post('frequency['.$example->id.']', true)
			);
			if($this->input->post('facility_id['.$example->id.']', true) != ""){
				$id = $this->Training_model->create_training_facility($data);
				$this->session->set_flashdata('success','Data Diklat fasilitas berhasil di simpan');
			}else{
				$this->session->set_flashdata('alert','Tidak ada data Diklat fasilitas yang dipilih');
			}
		}		
		redirect('trainings');
	}


	function create(){
		$data = array();
		$data['page_title'] = 'Data Diklat';
		$data['description'] = 'Informasi Data Diklat';
		$data['open_data_system'] = 'active';
		$data['active_data_master_training'] = 'active';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('major', 'major', 'required');	
		$this->form_validation->set_rules('semester', 'semester', 'required');	
		$this->form_validation->set_rules('training_specification', 'training_specification', 'required');	
		$this->form_validation->set_rules('accreditation', 'accreditation', 'required');	
		$this->form_validation->set_rules('no_mou', 'no_mou', 'required');	
		$this->form_validation->set_rules('date_cooperation_start', 'date_cooperation_start', 'required');	
		$this->form_validation->set_rules('date_cooperation_end', 'date_cooperation_end', 'required');	
		$this->form_validation->set_rules('mentor', 'mentor', 'required');	
		$this->form_validation->set_rules('contact_person', 'contact_person', 'required');	
		$this->form_validation->set_rules('phone', 'phone', 'required');	
		$this->form_validation->set_rules('training_date_start', 'training_date_start', 'required');	
		$this->form_validation->set_rules('training_date_end', 'training_date_end', 'required');	
		$this->form_validation->set_rules('participants', 'participants', 'required');	

		// FUNGSI UNTUK MELAKUKAN VALIDASI PESERTA DIKLAT PER HARI
		$filter = array(
			'training_date_start' => $this->input->post('training_date_start', true),
			'training_date_end' => $this->input->post('training_date_end', true)
		);
		$trainings_all = $this->Training_model->get_trainings($filter, null, null,'true');	
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'major' => $this->input->post('major', true),
				'semester' => $this->input->post('semester', true),
				'training_specification' => $this->input->post('training_specification', true),
				'accreditation' => $this->input->post('accreditation', true),
				'no_mou' => $this->input->post('no_mou', true),
				'date_cooperation_start' => $this->input->post('date_cooperation_start', true),
				'date_cooperation_end' => $this->input->post('date_cooperation_end', true),
				'mentor' => $this->input->post('mentor', true),
				'contact_person' => $this->input->post('contact_person', true),
				'phone' => $this->input->post('phone', true),
				'email' => $this->input->post('email', true),
				'training_date_start' => $this->input->post('training_date_start', true),
				'training_date_end' => $this->input->post('training_date_end', true),
				'participants' => $this->input->post('participants', true),
				'status' => $this->input->post('status', true),
				'address' => $this->input->post('address', true)
			);
			$this->middle = 'trainings/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'name' => $this->input->post('name', true),
				'major' => $this->input->post('major', true),
				'semester' => $this->input->post('semester', true),
				'training_specification' => $this->input->post('training_specification', true),
				'accreditation' => $this->input->post('accreditation', true),
				'no_mou' => $this->input->post('no_mou', true),
				'date_cooperation_start' => $this->input->post('date_cooperation_start', true),
				'date_cooperation_end' => $this->input->post('date_cooperation_end', true),
				'mentor' => $this->input->post('mentor', true),
				'contact_person' => $this->input->post('contact_person', true),
				'phone' => $this->input->post('phone', true),
				'email' => $this->input->post('email', true),
				'training_date_start' => $this->input->post('training_date_start', true),
				'training_date_end' => $this->input->post('training_date_end', true),
				'participants' => $this->input->post('participants', true),
				'status' => $this->input->post('status', true),
				'address' => $this->input->post('address', true),
				'user_id' => $this->simple_login->current_user()->id
			);
			$this->Training_model->id = $id;
			if ($trainings_all->result()[0]->sum_participants > 250){				
				$this->session->set_flashdata('alert','Gagal menyimpan! Peserta sudah melebihi kuota perhari');
			}else{				
				$id = $this->Training_model->create_training($data);
				if($this->input->post('status', true) == 'approve'){
					$this->Invoice_model->create_or_update_invoice($id);
					$this->session->set_flashdata('success','Data berhasil di disimpan dan mengirimkan invoice');
				}else{
					$this->session->set_flashdata('success','Data berhasil di simpan');
				}				
			}				
			redirect('trainings/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Training_model->id = $id;
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Detail Training',
			'description' => 'Informasi Detail Training',
			'training' => $this->Training_model->get_trainings()->row()
		);

		$this->middle = 'trainings/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Edit Training',
			'description' => 'Form Edit Training',
			'users' => $this->User_model->get_users()->row()
		);

		$this->Training_model->id = $this->uri->segment(3);
		$data['training'] = $this->Training_model->get_trainings()->row();
		$training = $data['training'];
		$data['value'] = array(
			'id' => $training->id,
			'name' => $training->name,
			'address' => $training->address,			
			'major' => $training->major,
			'semester' => $training->semester,
			'training_specification' => $training->training_specification,
			'accreditation' => $training->accreditation,
			'no_mou' => $training->no_mou,
			'date_cooperation_start' => $training->date_cooperation_start,
			'date_cooperation_end' => $training->date_cooperation_end,
			'mentor' => $training->mentor,
			'contact_person' => $training->contact_person,
			'phone' => $training->phone,
			'email' => $training->email,
			'training_date_start' => $training->training_date_start,
			'training_date_end' => $training->training_date_end,
			'participants' => $training->participants,
			'status' => $training->status
		);
		$this->middle = 'trainings/edit';
		$this->data = $data;
		$this->layout();
	}

	function edit_facility(){
		$this->Training_model->training_id = $this->input->get('training_id');
		$filter = "";
		$data = array(
			'open_data_system' => 'active',			
			'page_title' => 'Data Diklat Fasilitas',
			'description' => 'Tambah Data Diklat Fasilitas',
			'mdfacilities' => $this->Mdfacility_model->get_mdfacilities($filter, null, null,'true'),
			'training_facilities' => $this->Training_model->get_training_facilities()->result()
		);		
		$this->middle = 'trainings/facilities/edit';
		$this->data = $data;
		$this->layout();
	}

	function update_facility(){
		$filter = "";
		$data = array();
		$data['page_title'] = 'Edit Data Diklat Fasilitas';
		$data['description'] = 'Informasi Edit Data Diklat Fasilitas';
		$data['open_data_system'] = 'active';
		$data['active_data_master_training'] = 'active';		
		$mdfacilities = $this->Mdfacility_model->get_mdfacilities($filter, null, null,'true');
		foreach ($mdfacilities->result() as $example) {		
			$data = array(
				'id' => $this->input->post('id['.$example->id.']', true),
				'training_id' => $this->input->post('training_id', true),
				'facility_id' => $this->input->post('facility_id['.$example->id.']', true),
				'frequency' => $this->input->post('frequency['.$example->id.']', true),
				'created_at' => mdate('%Y-%m-%d'),
				'price' => $example->price * $this->input->post('frequency['.$example->id.']', true)
			);
			if($this->input->post('facility_id['.$example->id.']', true) != ""){				
				$id = $this->Training_model->update_training_facility($data);
				$this->session->set_flashdata('success','Edit Data Diklat fasilitas berhasil di simpan');
			}
		}		
		redirect('trainings');
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Diklat';
		$data['description'] = 'Informasi Data Diklat';
		$data['open_data_system'] = 'active';
		$data['active_data_training'] = 'active';

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');		

		// FUNGSI UNTUK MELAKUKAN VALIDASI PESERTA DIKLAT PER HARI
		$filter = array(
			'training_date_start' => $this->input->post('training_date_start', true),
			'training_date_end' => $this->input->post('training_date_end', true)
		);
		$trainings_all = $this->Training_model->get_trainings($filter, null, null,'true');		

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'name' => $this->input->post('name', true),
				'major' => $this->input->post('major', true),
				'semester' => $this->input->post('semester', true),
				'training_specification' => $this->input->post('training_specification', true),
				'accreditation' => $this->input->post('accreditation', true),
				'no_mou' => $this->input->post('no_mou', true),
				'date_cooperation_start' => $this->input->post('date_cooperation_start', true),
				'date_cooperation_end' => $this->input->post('date_cooperation_end', true),
				'mentor' => $this->input->post('mentor', true),
				'contact_person' => $this->input->post('contact_person', true),
				'phone' => $this->input->post('phone', true),
				'email' => $this->input->post('email', true),
				'training_date_start' => $this->input->post('training_date_start', true),
				'training_date_end' => $this->input->post('training_date_end', true),
				'participants' => $this->input->post('participants', true),
				'status' => $this->input->post('status', true),
				'address' => $this->input->post('address', true)
			);
			$this->middle = 'trainings/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'major' => $this->input->post('major', true),
				'semester' => $this->input->post('semester', true),
				'training_specification' => $this->input->post('training_specification', true),
				'accreditation' => $this->input->post('accreditation', true),
				'no_mou' => $this->input->post('no_mou', true),
				'date_cooperation_start' => $this->input->post('date_cooperation_start', true),
				'date_cooperation_end' => $this->input->post('date_cooperation_end', true),
				'mentor' => $this->input->post('mentor', true),
				'contact_person' => $this->input->post('contact_person', true),
				'phone' => $this->input->post('phone', true),
				'email' => $this->input->post('email', true),
				'training_date_start' => $this->input->post('training_date_start', true),
				'training_date_end' => $this->input->post('training_date_end', true),
				'participants' => $this->input->post('participants', true),
				'status' => $this->input->post('status', true),
				'address' => $this->input->post('address', true)
			);
			$this->Training_model->id = $id;
			if ($trainings_all->result()[0]->sum_participants > 250){				
				$this->session->set_flashdata('alert','Gagal menyimpan! Peserta sudah melebihi kuota perhari');
			}else{
				$this->Training_model->update_training($data);
				if($this->input->post('status', true) == 'approve'){
					$this->Invoice_model->create_or_update_invoice($id);
					$this->session->set_flashdata('success','Data berhasil di perbaharui dan mengirimkan invoice');
				}else{
					$this->session->set_flashdata('success','Data berhasil di perbaharui');
				}				
			}			
			redirect('trainings/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Training_model->id = $this->uri->segment(3);
		$this->Training_model->destroy();
		redirect('trainings/index');
	}

	function change(){
		//$this->simple_login->check_role();
		$data = array('status' => $this->input->get('approve', true));
		$this->Training_model->id = $this->uri->segment(3);
		$this->Training_model->update_training($data);
		redirect('trainings/index');	
	}
}