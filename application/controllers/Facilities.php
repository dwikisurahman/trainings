<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Facilities_model');
		$this->load->model('Training_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data Facilities',
			'description' => 'Informasi Data Example',
			'facilities_all' => $this->Facilities_model->get_facilities($filter, null, null,'true'),
			'trainings_all' => $this->Training_model->get_trainings($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'facilities/index/',
			'total_rows' => $data['facilities_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['facilitiess'] = $this->Facilities_model->get_facilities($filter, $config['per_page'], $from, null);
		$this->middle = 'trainings/facilities/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data Example',
			'description' => 'Tambah Data Example'
		);
		$this->middle = 'facilities/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data FAcilitiese';
		$data['description'] = 'Informasi Data Example';
		$data['open_data_master'] = 'active';
		$data['active_data_master_example'] = 'active';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'examples/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true),				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Example_model->create_example($data);
			redirect('facilities/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Facilities_model->id = $id;
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Detail Example',
			'description' => 'Informasi Detail Example',
			'facilities' => $this->Facilities_model->get_facilities()->row()
		);

		$this->middle = 'facilities/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Edit facilities',
			'description' => 'Form Edit facilities'
		);

		$this->Facilities_model->id = $this->uri->segment(3);
		$data['facilities'] = $this->Facilities_model->get_facilities()->row();
		$facilities = $data['facilities'];
		$data['value'] = array(
			'id' => $example->id,
			'name' => $example->name,
			'description' => $example->description
		);
		$this->middle = 'facilities/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Facilities';
		$data['description'] = 'Informasi Data Facilities';
		$data['open_data_master'] = 'active';
		$data['active_data_example'] = 'active';

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');		

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'facilities/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->Facilities_model->id = $id;
			$this->Facilities_model->update_facilities($data);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('facilities/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Facilities_model->id = $this->uri->segment(3);
		$this->Facilities_model->destroy();
		redirect('facilities/index');
	}
}