<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Role_model');
		$this->load->model('Module_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array(
			'name' => $this->input->get('name'),
			"description" => $this->input->get('description'),
			'date_join_start' => $this->input->get('date_join_start'),
			'date_join_end' => $this->input->get('date_join_end')
		);
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Data Posisi',
			'description' => 'Informasi Data Posisi',
			'roles_all' => $this->Role_model->get_roles($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'roles/index/',
			'total_rows' => $data['roles_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['roles'] = $this->Role_model->get_roles($filter, $config['per_page'], $from, null);
		$this->middle = 'roles/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Data Posisi',
			'description' => 'Tambah Data Posisi',
			'role_modules' => $this->Role_model->get_role_modules()
		);
		$this->middle = 'roles/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Posisi';
		$data['description'] = 'Informasi Data Posisi';
		$data['open_data_system'] = 'active';
		$data['active_data_system_role'] = 'active';
		$data['role_modules'] = $this->Role_model->get_role_modules();

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'roles/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true),
				'created_at' => mdate('%Y-%m-%d')
			);
			$insert = $this->Role_model->create_role($data);
			if ($insert){
				$this->Module_model->new_child($insert, true);
			}
			redirect('roles/show');
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Role_model->id = $id;
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Detail Posisi',
			'description' => 'Informasi Detail Posisi',
			'role' => $this->Role_model->get_roles()->row()
		);

		$this->middle = 'roles/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$this->Role_model->id = $this->uri->segment(3);
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Edit Posisi',
			'description' => 'Form Edit Posisi',
			'role' => $this->Role_model->get_roles()->row(),
			'role_modules' => $this->Role_model->get_role_modules()
		);
		$role = $data['role'];
		$data['value'] = array(
			'id' => $role->id,
			'name' => $role->name,
			'description' => $role->description
		);
		$this->middle = 'roles/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$id = $this->input->post('id', true);
		$this->Role_model->id = $id;
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Edit Posisi',
			'description' => 'Form Edit Posisi',
			'role' => $this->Role_model->get_roles()->row(),
			'role_modules' => $this->Role_model->get_role_modules()
		);

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $this->input->post('id', true),
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'roles/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'description' => $this->input->post('description', true)
			);
			$this->Role_model->id = $this->input->post('id', true);
			$role_modules = $this->Role_model->get_role_modules();
			foreach ($role_modules->result() as $role_module) {
				$name 		= $role_module->module_name;
				$post 		= $this->input->post(str_replace(" ","_",$name));
				if($role_module->module_application_id == $post{"module_application_id"}){
					$data_child = array(
						'is_read' => ((($post{"module_application_id"} == null) ? 0 : isset($post{"is_read"}) ? $post{"is_read"} == "on" ? 1 : 0 : 0)),
						'is_add' => ((($post{"module_application_id"} == null) ? 0 : isset($post{"is_add"}) ? $post{"is_add"} == "on" ? 1 : 0 : 0)),
						'is_update' => ((($post{"module_application_id"} == null) ? 0 : isset($post{"is_update"}) ? $post{"is_update"} == "on" ? 1 : 0 : 0)),
						'is_delete' => ((($post{"module_application_id"} == null) ? 0 : isset($post{"is_delete"}) ? $post{"is_delete"} == "on" ? 1 : 0 : 0)),
					);
					$this->Role_model->update_role_module($role_module->role_module_id, $data_child, $id);
				}
			}
			$this->Role_model->id = $id;
			$this->Role_model->update_role($data);
			redirect('roles/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Role_model->id = $this->uri->segment(3);
		$this->Role_model->destroy();
		redirect('roles/index');
	}
}