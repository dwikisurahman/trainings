<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdrooms extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Mdroom_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array();
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data Mdroom',
			'description' => 'Informasi Data Mdroom',
			'mdrooms_all' => $this->Mdroom_model->get_mdrooms($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'mdrooms/index/',
			'total_rows' => $data['mdrooms_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['mdrooms'] = $this->Mdroom_model->get_mdrooms($filter, $config['per_page'], $from, null);
		$this->middle = 'mdrooms/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Data mdroom',
			'description' => 'Tambah Data mdroom'
		);
		$this->middle = 'mdrooms/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Ruangan';
		$data['description'] = 'Informasi Data Ruangan';
		$data['open_data_master'] = 'active';
		$data['active_data_master_room'] = 'active';

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('room_name', 'room_name', 'required');
		$this->form_validation->set_rules('room_capacity', 'room_capacity', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'room_name' => $this->input->post('room_name', true),
				'room_capacity' => $this->input->post('room_capacity', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'mdrooms/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'created_at' => mdate('%Y-%m-%d'),
				'room_name' => $this->input->post('room_name', true),
				'room_capacity' => $this->input->post('room_capacity', true),
				'description' => $this->input->post('description', true)				
			);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			$id = $this->Mdroom_model->create_mdroom($data);
			redirect('mdrooms/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Mdroom_model->id = $id;
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Detail Mdroom',
			'description' => 'Informasi Detail Mdroom',
			'mdroom' => $this->Mdroom_model->get_mdrooms()->row()
		);

		$this->middle = 'mdrooms/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$data = array(
			'open_data_master' => 'active',			
			'page_title' => 'Edit Ruangan',
			'description' => 'Form Edit Ruangan'
		);

		$this->Mdroom_model->id = $this->uri->segment(3);
		$data['mdroom'] = $this->Mdroom_model->get_mdrooms()->row();
		$mdroom = $data['mdroom'];
		$data['value'] = array(
			'id' => $mdroom->id,
			'room_name' => $mdroom->room_name,
			'room_capacity' => $mdroom->room_capacity,
			'description' => $mdroom->description
		);
		$this->middle = 'mdrooms/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Ruangan';
		$data['description'] = 'Informasi Data Ruangan';
		$data['open_data_master'] = 'active';
		$data['active_data_example'] = 'active';

		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('room_name', 'room_name', 'required');
		$this->form_validation->set_rules('room_capacity', 'room_capacity', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'room_name' => $this->input->post('room_name', true),
				'room_capacity' => $this->input->post('room_capacity', true),
				'description' => $this->input->post('description', true),
			);
			$this->middle = 'mdrooms/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'room_name' => $this->input->post('room_name', true),
				'room_capacity' => $this->input->post('room_capacity', true),
				'description' => $this->input->post('description', true),
			);
			$this->Mdroom_model->id = $id;
			$this->Mdroom_model->update_mdroom($data);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('mdrooms/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Mdroom_model->id = $this->uri->segment(3);
		$this->Mdroom_model->destroy();
		redirect('mdrooms/index');
	}
}