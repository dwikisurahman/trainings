<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Invoice_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_invoices($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("invoices.*, trainings.name AS training_name, users.name AS user_name");
			$this->db->join("trainings", "trainings.id = invoices.training_id");
			$this->db->join("users", "users.id = invoices.user_id");
			$this->db->where("invoices.delete", "active");
			$this->db->from("invoices");
			$this->db->order_by("invoices.id DESC");
			if ($this->id) {
				$this->db->where("invoices.id", $this->id);
			}
			if (isset($filter['name']) && $filter['name'] != "") {
				$this->db->where("LOWER(users.name) LIKE '%".$filter['name']."%'");
			}

			// if (isset($filter['no_medtrack']) &&  $filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if (isset($filter['date_join_start']) &&  $filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }					
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_invoice($data){
			$this->db->where('id', $this->id);
			$this->db->update('invoices', $data);
		}

		function create_invoice($data){
			if($this->db->insert('invoices', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function create_or_update_invoice($training_id){
			$training = $this->db->query("SELECT * FROM trainings WHERE id = ".$training_id." LIMIT 1")->result()[0];
			$training_facilities = $this->db->query("SELECT *, SUM(price) AS sum_price FROM training_facilities WHERE training_id = ".$training->id." LIMIT 1")->result()[0];			
			$data = array(
				'training_id' => $training->id,
				'invoice_no' => "#i-0".$training_id,
				'total_amount' => $training_facilities->sum_price,
				'user_id' => $training->user_id
			);
			$invoice = $this->db->from("invoices")->where('training_id', $training_id);
			if ($invoice->get()->num_rows() == null) {
				$this->create_invoice($data);
			}else{
				$this->id = $invoice->id;
				$this->update_invoice($data);
			}
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('invoices', array('delete'=> 'deleted'));
		}

		public function insert($data)
            {
                $this->db->insert('students', $data);
                return TRUE;
            }

    public function view(){
        $query=$this->db->query("SELECT *
                                 FROM students
                                 ORDER BY id DESC");
        return $query->result_array();
    }
	}
?>