<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Training_model extends CI_Model {
		var $id;
		var $training_id;

		function __construct() {
			parent::__construct();
		}

		function get_trainings($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("trainings.*, users.id as user_id, users.role_id");			
			$this->db->from("trainings");
			$this->db->join("users", "users.id = trainings.user_id");
			$this->db->order_by("trainings.id DESC");
			if ($this->id) {
				$this->db->where("trainings.id", $this->id);
			}
			if (isset($filter['training_date_start']) && $filter['training_date_start'] != "") {
				$this->db->select("SUM(trainings.participants) AS sum_participants");	
				$this->db->where("trainings.training_date_start BETWEEN '".$filter['training_date_start']."' AND '".$filter['training_date_end']."' OR trainings.training_date_end BETWEEN '".$filter['training_date_end']."' AND '".$filter['training_date_start']."'");				
			}			
			
			$this->db->where("trainings.delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}			
			return $db;
		}

		function get_training_facilities(){
			$this->db->select("training_facilities.*");			
			$this->db->from("training_facilities");
			$this->db->order_by("id ASC");
			if ($this->training_id) {
				$this->db->where("training_id", $this->training_id);
			}

			$db = $this->db->get();				
			return $db;
		}
		function update_training_facility($data){
			$this->db->where('id', $data['id']);
			$this->db->update('training_facilities', $data);
		}

		function update_training($data){
			$this->db->where('id', $this->id);
			$this->db->update('trainings', $data);	
		}

		function create_training($data){
			if($this->db->insert('trainings', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function create_training_facility($data){
			if($this->db->insert('training_facilities', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        return FALSE;
		    }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('trainings', array('delete'=> 'deleted'));
		}
	}
?>