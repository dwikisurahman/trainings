<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Nursing_value_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_nursing_values($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("nursing_values.*");
			$this->db->where("delete", "active");
			$this->db->from("nursing_values");
			$this->db->order_by("id DESC");
			if ($this->id) {
				$this->db->where("id", $this->id);
			}
			if (isset($filter['name']) && $filter['name'] != "") {
				$this->db->where("LOWER(members.fullname) LIKE '%".$filter['name']."%'");
			}

			// if (isset($filter['no_medtrack']) &&  $filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if (isset($filter['date_join_start']) &&  $filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }
			
			$this->db->where("delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_nursing_value($data){
			$this->db->where('id', $this->id);
			$this->db->update('nursing_values', $data);
		}

		function create_nursing_value($data){
			if($this->db->insert('nursing_values', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('nursing_values', array('delete'=> 'deleted'));
		}
	}
?>