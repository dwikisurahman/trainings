<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Provcity_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_provinces($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("provinces.*");
			if ($this->id) {
				$this->db->where("provinces.id", $this->id);
			}

			$this->db->from("provinces");
			$this->db->order_by("id DESC");
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function get_cities($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("cities.*");
			if ($this->id) {
				$this->db->where("cities.id", $this->id);
			}
			
			if ($filter['province_id'] != "") {
				$this->db->where("province_id = ".$filter['province_id']);
			}

			$this->db->from("cities");
			$this->db->order_by("id DESC");
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}
	}
?>