<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Receipt_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_receipts($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("receipts.*, invoices.invoice_no, invoices.total_amount, trainings.name as training_name, invoices.status");
			$this->db->join("invoices", "invoices.id = receipts.invoice_id");
			$this->db->join("trainings", "trainings.id = receipts.training_id");
			$this->db->where("receipts.delete", "active");
			$this->db->from("receipts");
			$this->db->order_by("receipts.id DESC");
			if ($this->id) {
				$this->db->where("receipts.id", $this->id);
			}
			if (isset($filter['name']) && $filter['name'] != "") {
				$this->db->where("LOWER(members.fullname) LIKE '%".$filter['name']."%'");
			}

			// if (isset($filter['no_medtrack']) &&  $filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if (isset($filter['date_join_start']) &&  $filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }
			
			$this->db->where("receipts.delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_receipt($data){
			$this->db->where('id', $this->id);
			$this->db->update('receipts', $data);
		}

		function create_receipt($data){
			if($this->db->insert('receipts', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		public function get_data_selects($table){
		$res = false;
		try {
			$this->db->from($table);
			$this->table_record_count = $this->db->count_all_results();	

			$query = $this->db->get($table);
			
			if($query->num_rows() > 0){
				$res = $query->result_array();
			}

		} catch (Exception $e) {
			die($e->getMessage());
		}		
		return $res;
	}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('receipts', array('delete'=> 'deleted'));
		}
	}
?>