<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Mdfacility_model extends CI_Model {
		var $id;
		var $training_id;

		function __construct() {
			parent::__construct();
		}

		function get_mdfacilities($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("mdfacilities.*");
			$this->db->where("delete", "active");
			$this->db->from("mdfacilities");
			$this->db->order_by("id DESC");
			if ($this->id) {
				$this->db->where("id", $this->id);
			}
			// if ($filter['name'] != "") {
			// 	$this->db->where("LOWER(members.fullname) LIKE '%".$filter['name']."%'");
			// }

			// if ($filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if ($filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }
			
			$this->db->where("delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_mdfacility($data){
			$this->db->where('id', $this->id);
			$this->db->update('mdfacilities', $data);
		}

		function create_mdfacility($data){
			if($this->db->insert('mdfacilities', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('mdfacilities', array('delete'=> 'deleted'));
		}
	}
?>