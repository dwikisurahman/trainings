<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Student_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_students($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("students.*, trainings.name as training_name");
			$this->db->join("trainings", "trainings.id = students.training_id");
			$this->db->where("students.delete", "active");
			$this->db->from("students");
			$this->db->order_by("students.id DESC");
			if ($this->id) {
				$this->db->where("students.id", $this->id);
			}
			if (isset($filter['name']) && $filter['name'] != "") {
				$this->db->where("LOWER(members.fullname) LIKE '%".$filter['name']."%'");
			}

			// if (isset($filter['no_medtrack']) &&  $filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if (isset($filter['date_join_start']) &&  $filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }
			
			$this->db->where("students.delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_students($data){
			$this->db->where('id', $this->id);
			$this->db->update('students', $data);
		}

		function create_students($data){
			if($this->db->insert('students', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		public function get_data_selects($table){
		$res = false;
		try {
			$this->db->from($table);
			$this->table_record_count = $this->db->count_all_results();	

			$query = $this->db->get($table);
			
			if($query->num_rows() > 0){
				$res = $query->result_array();
			}

		} catch (Exception $e) {
			die($e->getMessage());
		}		
		return $res;
	}

		function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }
    
    
     // * Insert members data into the database
     // * @param $data data to be insert based on the passed parameters
     
    public function insert($data = array()) {
        if(!empty($data)){
            // Add created and modified date if not included
            if(!array_key_exists("created_at", $data)){
                $data['created_at'] = date("Y-m-d H:i:s");
            }
            
            // Insert member data
            $insert = $this->db->insert($this->table, $data);
            
            // Return the status
            return $insert?$this->db->insert_id():false;
        }
        return false;
    }
    
    /*
     * Update member data into the database
     * @param $data array to be update based on the passed parameters
     * @param $condition array filter data
     */
    public function update($data, $condition = array()) {
        if(!empty($data)){
            // Add modified date if not included
            if(!array_key_exists("modified", $data)){
                $data['modified'] = date("Y-m-d H:i:s");
            }
            
            // Update member data
            $update = $this->db->update($this->table, $data, $condition);
            
            // Return the status
            return $update?true:false;
        }
        return false;
    }

	}
?>