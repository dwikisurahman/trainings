<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Schedule_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_schedules($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("schedules.*, mdrooms.room_name, mdrooms.room_capacity");
			$this->db->join("mdrooms", "mdrooms.id = schedules.mdroom_id");
			$this->db->where("schedules.delete", "active");
			$this->db->from("schedules");
			$this->db->order_by("schedules.id DESC");
			if ($this->id) {
				$this->db->where("schedules.id", $this->id);
			}
			if (isset($filter['name']) && $filter['name'] != "") {
				$this->db->where("LOWER(members.fullname) LIKE '%".$filter['name']."%'");
			}

			// if (isset($filter['no_medtrack']) &&  $filter['no_medtrack'] != "") {
			// 	$this->db->where("members.no_medtrack", $filter['no_medtrack']);
			// }

			// if (isset($filter['date_join_start']) &&  $filter['date_join_start'] != "") {
			// 	$this->db->where("members.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			// }
			
			$this->db->where("schedules.delete", "active");
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_schedule($data){
			$this->db->where('id', $this->id);
			$this->db->update('schedules', $data);
		}

		function create_schedule($data){
			if($this->db->insert('schedules', $data)){
				$insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('schedules', array('delete'=> 'deleted'));
		}
	}
?>