<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Module_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_modules($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("modules.*");
			$this->db->where("delete", "active");
			if ($this->id ) {
				$this->db->where("id", $this->id);
			}
			if ($filter['name'] != "") {
				$this->db->where("LOWER(name) LIKE '%".$filter['name']."%'");
			}

			if ($filter['path'] != "") {
				$this->db->where("path_module", $filter['path']);
			}

			$this->db->from("modules");
			$this->db->order_by("id DESC");
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			
			return $db;
		}

		function get_childs(){
			$this->db->select("modules.*");
			$this->db->from("modules");
			$this->db->where("delete", "active");
			$this->db->where("child", null);
			$db = $this->db->get();
			return $db;
		}

		function update_module($data){
			$this->db->where('id', $this->id);
			$this->db->update('modules', $data);
		}

		function create_module($data){
			if($this->db->insert('modules', $data)){
	            $insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('modules', array('delete'=> 'deleted'));
		}

		function new_child($role_id=null,$global=null){
			if ($global == null) {
				$row = $this->db->query('SELECT * FROM modules ORDER BY id DESC LIMIT 1');
				$module_labels = $row->result();
			}else{
				$row = $this->db->query('SELECT * FROM modules ORDER BY id DESC');
				$module_labels = $row->result();	
				$row = $this->db->query('SELECT COUNT(*) AS count FROM modules');
				$row_app = $this->db->query('SELECT * FROM module_applications ORDER BY id DESC LIMIT '.$row->row()->count);
				$module_applications = $row_app->result();
			}
			
			foreach ($module_labels as $module_label) {
				$mod_app = array(
					"module_id"=> $module_label->id, 
					"action_read"=> "can :read, ".$module_label->model_class,
					"action_create"=> "can :create, ".$module_label->model_class,
					"action_update"=> "can :update, ".$module_label->model_class,
					"action_destroy"=> "can :destroy, ".$module_label->model_class
				);
				$this->db->insert("module_applications", $mod_app);
			}
			if ($global == null) {
				$row_app = $this->db->query('SELECT * FROM module_applications ORDER BY id DESC LIMIT 1');
				$module_applications = $row_app->result();
			}
			foreach ($module_applications as $module_application) {
				$role_module = array(
					"role_id" => ($role_id == null ? 1 : $role_id), 
					"module_application_id" => $module_application->id, 
					"is_read" => 1, 
					"is_add" => 1, 
					"is_update" => 1, 
					"is_delete" => 1
				);
				$this->db->insert("role_modules", $role_module);
			}
		}
	}
?>